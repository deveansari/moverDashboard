@extends('admin.layouts.app')
@section('title', 'create Mover')

@section('goBody')

<div id="app">
    <h5>Add New Moving Companies</h5>
    <p>To add new movers fill up the form bellow <span class="bg-warning">Once you have created a new movers you need
            to add CHAMPAIGN to send lead to movers.</span>
    </p>
    <hr>
    <h5>Moving CoMpany Information <span class="text-muted">enter company information below</span></h5>
    <hr>
    @include('admin.includes.messages')
    {{--@{{message}}--}}

    <form action="/admin/mover/@yield('editId')" method="post" class="">
        {{csrf_field()}}
        @section('editMethod')
        @show
        <fieldset>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="userName">User Name*</label>
                    <div class="col-md-4">
                        <input type="text" name="userName" id="title" class="form-control" value="@yield('userName')"
                            placeholder="userName" aria-describedby="helpId">
                    </div>
                    <div class="col-md-5">
                        <label class="control-label"><span class="text-info text-muted">Enter Mover User Name</span></label>
                    </div>
                </div>
                <hr class="dotted">
                <div class="form-group">
                    <label for="password" class="col-md-3 control-label">User Password</label>
                    <div class="col-md-4">
                        <input type="text" name="password" id="title" class="form-control" value="@yield('password')"
                            placeholder="password" aria-describedby="helpId">
                    </div>
                    <div class="col-md-5">
                        <label class="control-label"><span class="text-info text-muted">Enter Mover Password</span></label>
                    </div>
                </div>
                <hr class="dotted">
            </div>


            <h5 class="text-success">User More Information <span class="text-muted">enter company information below</span></h5>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="moverName">Movers Company Name <span class="text-muted">Exp. Arrow Moving</span></label>
                        <input type="text" name="moverName" id="title" class="form-control" value="@yield('moverName')"
                            placeholder="moverName" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="usDotNumber">USDot Number <span class="text-muted">Exp. 17371</span></label>
                        <input type="text" name="usDotNumber" id="title" class="form-control" value="@yield('usDotNumber')"
                            placeholder="usDotNumber" aria-describedby="helpId">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ownerName">Movers Owner's Name* <span class="text-muted">Exp. Nicole Ford</span></label>
                        <input type="text" name="ownerName" id="title" class="form-control" value="@yield('ownerName')"
                            placeholder="ownerName" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="ownerMccNum">Movers MCC Number</label>
                        <input type="text" name="ownerMccNum" id="title" class="form-control" value="@yield('ownerMccNum')"
                            placeholder="ownerMccNum" aria-describedby="helpId">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="contactName">Movers Contact Name* <span class="text-muted">Exp. John Hashi</span></label>
                        <input type="text" name="contactName" id="title" class="form-control" value="@yield('contactName')"
                            placeholder="contactName" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="otherLcnInfo">Movers Other licence Info</label>
                        <input type="text" name="otherLcnInfo" id="title" class="form-control" value="@yield('otherLcnInfo')"
                            placeholder="otherLcnInfo" aria-describedby="helpId">
                    </div>
                </div>


            </div>
            <hr class="dotted">

            <h5 class="text-success">Moving Partner Info</h5>
            <hr>
            <div class="form-horizontal">
                <div class="">
                    <div class="form-group">
                        <label for="gnrlEmail" class="col-md-3 control-label">General Emil*</label>
                        <div class="col-md-4">
                            <input type="text" name="gnrlEmail" id="title" class="form-control" value="@yield('gnrlEmail')"
                                placeholder="gnrlEmail" aria-describedby="helpId">
                        </div>
                        <div class="col-md-5">
                            <label class="control-label"><span class="text-info text-muted">Would be use for
                                    communication</span></label>
                        </div>
                    </div>
                    <hr class="dotted">
                    <div class="form-group">
                        <label for="leadCollectEmail" class="col-md-3 control-label">Lead Recevining Email*</label>
                        <div class="col-md-4">
                            <input type="text" name="leadCollectEmail" id="title" class="form-control" value="@yield('leadCollectEmail')"
                                placeholder="leadCollectEmail" aria-describedby="helpId">
                        </div>
                        <div class="col-md-5">
                            <label class="control-label"><span class="text-info text-muted">This email will receive
                                    leads</span></label>
                        </div>
                    </div>
                    <hr class="dotted">
                </div>
            </div>
            <h5 class="text-success">Lead Receiving Settings</h5>
            <hr>
            <div class="row">
                <div class="col-md-12">

                    <div class="checkbox-custom checkbox-inline">
                        <input id="enable" type="checkbox" v-model="enable" name="chkSendList" value="checked" @click="click"
                            @yield('chkSendList')>
                        <label for="chkSendList">Send List</label>
                    </div>


                    <span v-if="show" {{-- @yield('show') --}}>
                        <div class="radio-custom radio-inline">
                            <input type="radio" :disabled="!enable" name="chkFrmtHtml" value="1" @yield('chkFrmtHtml')>
                            <label for="chkFrmtHtml">( HTML <span class="text-danger">or</span></label>

                        </div>
                        <div class="radio-custom radio-inline">
                            <input type="radio" :disabled="!enable" name="chkFrmtHtml" value="0" @yield('plain')>
                            <label for="chkFrmtHtml">Plain ) via Email. <span class="text-muted bg-warning">Will send
                                    Leads Data only to email Address</span></label>
                        </div>
                    </span>
                    <div class="moveritem">

                        <ol>

                            {{-- <mover-item v-for="item in options" v-bind:key="options.id" v-bind:options="item"></mover-item>--}}
                            {{-- <mover-item v-for="item in options" v-bind:options="item" v-bind:key="item.id"></mover-item>--}}
                        </ol>
                    </div>


                </div>
            </div>
            <hr class="dotted">

            <h5 class="text-success">Send SMS to Mover Cell Phone <span class="text-muted">if checked then a SMS will
                    send Movers cell Number</span>
            </h5>
            <hr>
            <div class="form-horizontal left">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="checkbox-custom checkbox-inline">
                                <input name="checkSmsPhn" v-model="activeSmsinput" type="checkbox" value="checked"
                                    @yield('checkSmsPhn')>
                                <label for="checkSmsPhn">Send SMS To Movers Cell Phone</label>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="smsNumber" id="title" class="form-control" :disabled="!activeSmsinput"
                                value="@yield('smsNumber')" placeholder="smsNumber" aria-describedby="helpId">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="control-label"><span class="text-info">Exp. 3472622404. Just Plain Number. NO space,
                                no - or +, no (or), no leading 0 or 1.</span>
                        </p>
                    </div>
                    <hr class="dotted">
                </div>

            </div>


            <h5 class="text-success">Send To GRANOT <span class="text-muted">send list to via granot</span></h5>
            <hr class="dotted">
            <div class="row">
                <div class="col-md-12">
                    <div class="checkbox-custom checkbox-inline">
                        <input name="chkGranot" type="checkbox" value="checked" @yield('chkGranot')>
                        <label for="chkGranot">Send Lead Data To Customer Granot only ( Note: Not Car Data )</label>
                    </div>
                </div>

            </div>
            <hr class="dotted">

            <h5 class="text-success">Lead Pricing <span class="text-muted">Enter Long and Local Pricing Below</span>
            </h5>
            <hr>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="longLeadPrice" class="col-md-3 control-label">Long Lead Price*</label>
                    <div class="col-md-2">
                        <input type="text" name="longLeadPrice" class="form-control" value="@yield('longLeadPrice')"
                            placeholder="longLeadPrice" aria-describedby="helpId">
                    </div>
                    <div class="col-md-7">
                        <label class="control-label"><span class="text-info text-muted">how much to change for long
                                leads</span></label>
                    </div>
                </div>
                <hr class="dotted">
                <div class="form-group">
                    <label for="localLeadPrice" class="col-md-3 control-label">Local Lead Price*</label>
                    <div class="col-md-2">
                        <input type="text" name="localLeadPrice" class="form-control" value="@yield('localLeadPrice')"
                            placeholder="localLeadPrice" aria-describedby="helpId">
                    </div>
                    <div class="col-md-7">
                        <label class="control-label"><span class="text-info text-muted">how much to change for long
                                leads</span></label>
                    </div>
                </div>
                <hr class="dotted">
            </div>

            <h5 class="text-success">Lead Per Hour <span class="text-muted">Enter how many leads movers wants per hour
                    basis</span>
                <span class="bg-warning text-muted">
                    enter 0 for unlimited
                </span></h5>
            <hr>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="leadPerHour" class="col-md-3 control-label">How Many leads Per Hour*</label>
                    <div class="col-md-2">
                        <input type="text" name="leadPerHour" class="form-control" value="@yield('leadPerHour')"
                            placeholder="leadPerHour" aria-describedby="helpId">
                    </div>
                    <div class="col-md-7">
                        <label class="control-label"><span class="text-info text-muted">put 0 to unlimited</span></label>
                    </div>
                </div>
                <hr class="dotted">
            </div>
            {{--newly inserted--}}
            <h5 class="text-success">Want Car Leads Data <span class="text-muted bg-warning">check it ON if you want
                    send car leads data so client might get 2 email 1 for normal move and 1 for car move</span>
            </h5>

            <hr>
            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-12">
                        <div class="checkbox-custom checkbox-inline">
                            <input name="chkcarleadsEmail" type="checkbox" value="checked" @yield('chkcarleadsEmail')>
                            <label for="chkcarleadsEmail">Send Car Leads Data to Customers Email Address
                                Only</label>
                        </div>
                    </div>

                </div>
                <hr class="dotted">
                <div class="row">
                    <div class="col-md-12">
                        <div class="checkbox-custom checkbox-inline">
                            <input name="chkcarleadsGranot" type="checkbox" value="checked" @yield('chkcarleadsGranot')>
                            <label for="chkcarleadsGranot">Send Car Leads Data to Customer Granot <span class="bg-warning text-muted">(
                                    Car Data Only Not Regular Lead. Lead receiving email will be use for Granot )</span></label>
                        </div>
                    </div>

                </div>
                <hr class="dotted">
            </div>


            <h5 class="text-success">Car Lead Pricing <span class="text-muted">Enter Long and Local Pricing Below</span>
            </h5>
            <hr>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="longCarLeadPrice" class="col-md-3 control-label">Long Car Lead Price*</label>
                    <div class="col-md-2">
                        <input type="text" name="longCarLeadPrice" class="form-control" value="@yield('longCarLeadPrice')"
                            placeholder="longCarLeadPrice" aria-describedby="helpId">
                    </div>
                    <div class="col-md-7">
                        <label class="control-label"><span class="text-info text-muted">how much to change for long car
                                leads</span></label>
                    </div>
                </div>
                <hr class="dotted">
                <div class="form-group">
                    <label for="localCarLeadPrice" class="col-md-3 control-label">Local Car Lead Price*</label>
                    <div class="col-md-2">
                        <input type="text" name="localCarLeadPrice" class="form-control" value="@yield('localCarLeadPrice')"
                            placeholder="localCarLeadPrice" aria-describedby="helpId">
                    </div>
                    <div class="col-md-7">
                        <label class="control-label"><span class="text-info text-muted">how much to change for long car
                                leads</span></label>
                    </div>
                </div>
                <hr class="dotted">
            </div>

            <h5 class="text-success">Prohabitted States For Leads</h5>
            <p class="text-muted">If a mover does not want any leads from a specific state or does not want to move
                to a specific state then mention it here. it is useful for zip and surrounding miles champaign
                settings.</p>
            <hr>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="disallowLeadStates" class="col-md-3 control-label">Do Not Want Lead From These
                        States</label>
                    <div class="col-md-4">
                        <span v-for="item in statesName">@{{item}}, </span>

                        <input type="text" name="disallowLeadStates" class="form-control" v-model="alreadySelectedLeadStates"
                            placeholder="Add States To Disallow" {{--value="--}}
                            {{--@foreach($stateSelected as $value){{$value}} , @endforeach--}}{{--"--}}
                            aria-describedby="helpId">

                        {{--
                        @{{ alreadySelectedLeadStates }}
                        <ul>
                            <li v-for="item in alreadySelectedLeadStates">
                                @{{ item }}
                            </li>
                        </ul>
                        --}}

                    </div>
                    <div class="col-md-5">
                        <div v-show="preClickstate" class="disallowStates">
                            <div class="stateListContainerWrapper" style="position: relative">
                                <div class="btnGroup" style="position: absolute">
                                    <button type="button" class="btn btn-info" v-on:click="preClickstate = false"><b>Ok
                                            Done!</b>
                                    </button>
                                    {{--<span class="pull-right" v-on:click="this.$data.preClickstate = false">&times;</span>--}}

                                </div>
                                <button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true" v-on:click="preClickstate = false">&times;</span>
                                </button>
                                <div class="stateListContainer">


                                    <ul class="list-group" style="columns: 3;padding: 0px 0 15px 15px;">
                                        <li class="" v-for="item in states">
                                            <div class="checkbox-custom checkbox-inline">
                                                <input type="checkbox" v-model="alreadySelectedLeadStates" name="disallowLeadStates[]"
                                                    :value="item.name">
                                                <label :for="item.name"> @{{ item.name }}</label>
                                            </div>

                                        </li>

                                    </ul>
                                </div>
                            </div>



                        </div>


                        <button type="button" class="btn btn-default mr5 mb10 btn-warning" @click="preStateTrue">
                            <i class="fa fa-plus"></i> States
                        </button>
                    </div>


                </div>
                <hr class="dotted">
                <div class="form-group">
                    <label for="disallowMovingStates" class="col-md-3 control-label">Do Not Want Leads Moving TO
                        These States</label>
                    <div class="col-md-4">
                        <input type="text" name="disallowMovingStates" class="form-control" v-model="alreadySelectedMovingStates"
                            {{-- value="@yield('disallowMovingStates')" --}} placeholder="disallowMovingStates"
                            aria-describedby="helpId">
                    </div>
                    <div class="col-md-5">
                        <div v-show="disallowMovingStatesWindowSwitcher" class="">
                            <div class="stateListContainerWrapper" style="position: relative">
                                <div class="btnGroup" style="position: absolute">
                                    <button type="button" class="btn btn-info" v-on:click="disallowMovingStatesWindowSwitcher = false"><b>Ok
                                            Done!</b>
                                    </button>
                                    {{--<span class="pull-right" v-on:click="this.$data.preClickstate = false">&times;</span>--}}

                                </div>
                                <button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true" v-on:click="disallowMovingStatesWindowSwitcher = false">&times;</span>
                                </button>
                                <div class="stateListContainer">


                                    <ul class="list-group" style="columns: 3;padding: 0px 0 15px 15px;">
                                        <li class="" v-for="item in states">
                                            <div class="checkbox-custom checkbox-inline">
                                                <input type="checkbox" v-model="alreadySelectedMovingStates" name="disallowMovingStates[]"
                                                    :value="item.name">
                                                <label :for="item.name"> @{{ item.name }}</label>
                                            </div>

                                        </li>

                                    </ul>
                                </div>
                            </div>



                        </div>


                        <button type="button" class="btn btn-default mr5 mb10 btn-warning" @click="disallowMovingStatesWindowTrue">
                            <i class="fa fa-plus"></i> States
                        </button>

                    </div>

                </div>
                <hr class="dotted">

            </div>

            {{--newly inserted--}}
            <h5 class="text-success">Moving Company Address Informarion <span class="text-muted">enter company address
                    information below</span>
            </h5>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="moverAdd">mover Address</label>
                        <input type="text" name="moverAdd" id="title" class="form-control" value="@yield('moverAdd')"
                            placeholder="moverAdd" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="zipCode">Zip Code</label>
                        <input type="text" name="zipCode" id="title" class="form-control" value="@yield('zipCode')"
                            placeholder="zipCode" aria-describedby="helpId">
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="moverCity">mover City</label>
                        {{-- <input type="text" name="moverCity" id="title" class="form-control" value="@yield('moverCity')"
                            placeholder="moverCity" aria-describedby="helpId"> --}}


                        <select class="form-control" name="moverCity" v-model="moverCityName">
                            <optgroup label="Prev. City @yield('moverCity')">

                                <option class="" v-for="(option, index) in statesAndCity[moverStateName]" :value="option">@{{option}}</option>

                            </optgroup>
                        </select>

                    </div>

                    <div class="form-group">
                        <label for="moverPhoneNumber">Mover Phone Number</label>
                        <input type="text" name="moverPhoneNumber" id="title" class="form-control" value="@yield('moverPhoneNumber')"
                            placeholder="moverPhoneNumber" aria-describedby="helpId">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="moverState">Mover State</label>
                        {{-- <select class="form-control" name="moverState">
                            <optgroup label="Prv. Selection @yield('moverState')">

                                <option class="" v-for="item in states" :value="item.name">@{{item.name}}</option>

                            </optgroup>
                        </select> --}}
                        <select class="form-control" name="moverState" v-model="moverStateName">
                            <optgroup label="Selected State @yield('moverState')">

                                <option class="" v-for="(item, index) in statesAndCity" :value="index">@{{index}}</option>

                            </optgroup>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="moverFaxNumber">Mover Fax Number</label>
                        <input type="text" name="moverFaxNumber" id="title" class="form-control" value="@yield('moverFaxNumber')"
                            placeholder="moverFaxNumber">
                    </div>
                </div>


            </div>
            <hr class="dotted">

            {{--newly inserted--}}
            <h5 class="text-success">Lead Receiving Settings</h5>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="checkbox-custom checkbox-inline">
                        <input type="checkbox" name="chkPaypal" value="checked" @yield('chkPaypal')>
                        <label for="chkPaypal">Paying Via Paypal</label>
                    </div>

                    <div class="checkbox-custom checkbox-inline">
                        <input type="checkbox" name="chkCc" value="checked" @yield('chkCc')>
                        <label for="chkCc">Paying Via CC</label>
                    </div>


                </div>
            </div>
            <hr class="dotted">

            {{--newly inserted--}}
            <h5 class="text-success">Mover commpany Credit Card Informarion</h5>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="cardHolderName">Card Holder Name</label>
                        <input type="text" name="cardHolderName" id="title" class="form-control" value="@yield('cardHolderName')"
                            placeholder="cardHolderName" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="cardExpMonth">Expiration Month</label>
                        <select class="form-control" name="cardExpMonth">
                            <optgroup label="Pacific Time Zone">
                                <option value="CA">cardExpMonth</option>
                                <option value="NV">Nevada</option>
                                <option value="OR">Oregon</option>
                                <option value="WA">Washington</option>
                            </optgroup>
                        </select>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ccNumber">CC Number</label>
                        <input type="text" name="ccNumber" id="title" class="form-control" value="@yield('ccNumber')"
                            placeholder="ccNumber" aria-describedby="helpId">
                    </div>

                    <div class="form-group">
                        <label for="cardExpYear">Expiration Year</label>
                        <select class="form-control" name="cardExpYear">
                            <optgroup label="Pacific Time Zone">
                                <option value="CA">cardExpYear</option>
                                <option value="NV">Nevada</option>
                                <option value="OR">Oregon</option>
                                <option value="WA">Washington</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    {{-- @{{$firstTelvDigit}}--}}
                    <div class="form-group">
                        <label for="cwNumber">CW Number</label>
                        <input type="text" name="cwNumber" id="title" class="form-control" value="@yield('cwNumber')"
                            placeholder="cwNumber" aria-describedby="helpId">
                    </div>

                </div>


            </div>
            <hr class="dotted">
            <h5 class="text-success">Enter details/remarks about customer <span class="text-muted">html enable help
                    text. be aware about html tag completion </span>
            </h5>
            <hr>
            <textarea id="summernote" name="detailsCustomer" class="note-codable" placeholder="editor" aria-describedby="helpId"> @yield('detailsCustomer')</textarea>

            <br>
            <button type="submit" class="btn btn-primary">Submit</button>

        </fieldset>


    </form>





</div>


<script>
    new Vue({
        el: '#app',
        data: {

            message: 'Hello Sir!',
            enable: false,
            show: false,
            preClickstate: false,
            disallowMovingStatesWindowSwitcher: false,
            state: true,
            selected: [], // Must be an array reference!
            myString: [],
            alreadySelectedLeadStates: [],
            alreadySelectedMovingStates: [],
            watchingSomething: [],
            statesName: [],
            moverStateName: null,
            moverCityName: null,
            states: [{
                    "name": "Alabama",
                    "abbreviation": "AL"
                },
                {
                    "name": "Alaska",
                    "abbreviation": "AK"
                },
                {
                    "name": "American Samoa",
                    "abbreviation": "AS"
                },
                {
                    "name": "Arizona",
                    "abbreviation": "AZ"
                },
                {
                    "name": "Arkansas",
                    "abbreviation": "AR"
                },
                {
                    "name": "California",
                    "abbreviation": "CA"
                },
                {
                    "name": "Colorado",
                    "abbreviation": "CO"
                },
                {
                    "name": "Connecticut",
                    "abbreviation": "CT"
                },
                {
                    "name": "Delaware",
                    "abbreviation": "DE"
                },
                {
                    "name": "District Of Columbia",
                    "abbreviation": "DC"
                },
                {
                    "name": "Federated States Of Micronesia",
                    "abbreviation": "FM"
                },
                {
                    "name": "Florida",
                    "abbreviation": "FL"
                },
                {
                    "name": "Georgia",
                    "abbreviation": "GA"
                },
                {
                    "name": "Guam",
                    "abbreviation": "GU"
                },
                {
                    "name": "Hawaii",
                    "abbreviation": "HI"
                },
                {
                    "name": "Idaho",
                    "abbreviation": "ID"
                },
                {
                    "name": "Illinois",
                    "abbreviation": "IL"
                },
                {
                    "name": "Indiana",
                    "abbreviation": "IN"
                },
                {
                    "name": "Iowa",
                    "abbreviation": "IA"
                },
                {
                    "name": "Kansas",
                    "abbreviation": "KS"
                },
                {
                    "name": "Kentucky",
                    "abbreviation": "KY"
                },
                {
                    "name": "Louisiana",
                    "abbreviation": "LA"
                },
                {
                    "name": "Maine",
                    "abbreviation": "ME"
                },
                {
                    "name": "Marshall Islands",
                    "abbreviation": "MH"
                },
                {
                    "name": "Maryland",
                    "abbreviation": "MD"
                },
                {
                    "name": "Massachusetts",
                    "abbreviation": "MA"
                },
                {
                    "name": "Michigan",
                    "abbreviation": "MI"
                },
                {
                    "name": "Minnesota",
                    "abbreviation": "MN"
                },
                {
                    "name": "Mississippi",
                    "abbreviation": "MS"
                },
                {
                    "name": "Missouri",
                    "abbreviation": "MO"
                },
                {
                    "name": "Montana",
                    "abbreviation": "MT"
                },
                {
                    "name": "Nebraska",
                    "abbreviation": "NE"
                },
                {
                    "name": "Nevada",
                    "abbreviation": "NV"
                },
                {
                    "name": "New Hampshire",
                    "abbreviation": "NH"
                },
                {
                    "name": "New Jersey",
                    "abbreviation": "NJ"
                },
                {
                    "name": "New Mexico",
                    "abbreviation": "NM"
                },
                {
                    "name": "New York",
                    "abbreviation": "NY"
                },
                {
                    "name": "North Carolina",
                    "abbreviation": "NC"
                },
                {
                    "name": "North Dakota",
                    "abbreviation": "ND"
                },
                {
                    "name": "Northern Mariana Islands",
                    "abbreviation": "MP"
                },
                {
                    "name": "Ohio",
                    "abbreviation": "OH"
                },
                {
                    "name": "Oklahoma",
                    "abbreviation": "OK"
                },
                {
                    "name": "Oregon",
                    "abbreviation": "OR"
                },
                {
                    "name": "Palau",
                    "abbreviation": "PW"
                },
                {
                    "name": "Pennsylvania",
                    "abbreviation": "PA"
                },
                {
                    "name": "Puerto Rico",
                    "abbreviation": "PR"
                },
                {
                    "name": "Rhode Island",
                    "abbreviation": "RI"
                },
                {
                    "name": "South Carolina",
                    "abbreviation": "SC"
                },
                {
                    "name": "South Dakota",
                    "abbreviation": "SD"
                },
                {
                    "name": "Tennessee",
                    "abbreviation": "TN"
                },
                {
                    "name": "Texas",
                    "abbreviation": "TX"
                },
                {
                    "name": "Utah",
                    "abbreviation": "UT"
                },
                {
                    "name": "Vermont",
                    "abbreviation": "VT"
                },
                {
                    "name": "Virgin Islands",
                    "abbreviation": "VI"
                },
                {
                    "name": "Virginia",
                    "abbreviation": "VA"
                },
                {
                    "name": "Washington",
                    "abbreviation": "WA"
                },
                {
                    "name": "West Virginia",
                    "abbreviation": "WV"
                },
                {
                    "name": "Wisconsin",
                    "abbreviation": "WI"
                },
                {
                    "name": "Wyoming",
                    "abbreviation": "WY"
                }
            ],
            statesAndCity: {
                "New York": [
                    "New York",
                    "Buffalo",
                    "Rochester",
                    "Yonkers",
                    "Syracuse",
                    "Albany",
                    "New Rochelle",
                    "Mount Vernon",
                    "Schenectady",
                    "Utica",
                    "White Plains",
                    "Hempstead",
                    "Troy",
                    "Niagara Falls",
                    "Binghamton",
                    "Freeport",
                    "Valley Stream"
                ],
                "California": [
                    "Los Angeles",
                    "San Diego",
                    "San Jose",
                    "San Francisco",
                    "Fresno",
                    "Sacramento",
                    "Long Beach",
                    "Oakland",
                    "Bakersfield",
                    "Anaheim",
                    "Santa Ana",
                    "Riverside",
                    "Stockton",
                    "Chula Vista",
                    "Irvine",
                    "Fremont",
                    "San Bernardino",
                    "Modesto",
                    "Fontana",
                    "Oxnard",
                    "Moreno Valley",
                    "Huntington Beach",
                    "Glendale",
                    "Santa Clarita",
                    "Garden Grove",
                    "Oceanside",
                    "Rancho Cucamonga",
                    "Santa Rosa",
                    "Ontario",
                    "Lancaster",
                    "Elk Grove",
                    "Corona",
                    "Palmdale",
                    "Salinas",
                    "Pomona",
                    "Hayward",
                    "Escondido",
                    "Torrance",
                    "Sunnyvale",
                    "Orange",
                    "Fullerton",
                    "Pasadena",
                    "Thousand Oaks",
                    "Visalia",
                    "Simi Valley",
                    "Concord",
                    "Roseville",
                    "Victorville",
                    "Santa Clara",
                    "Vallejo",
                    "Berkeley",
                    "El Monte",
                    "Downey",
                    "Costa Mesa",
                    "Inglewood",
                    "Carlsbad",
                    "San Buenaventura (Ventura)",
                    "Fairfield",
                    "West Covina",
                    "Murrieta",
                    "Richmond",
                    "Norwalk",
                    "Antioch",
                    "Temecula",
                    "Burbank",
                    "Daly City",
                    "Rialto",
                    "Santa Maria",
                    "El Cajon",
                    "San Mateo",
                    "Clovis",
                    "Compton",
                    "Jurupa Valley",
                    "Vista",
                    "South Gate",
                    "Mission Viejo",
                    "Vacaville",
                    "Carson",
                    "Hesperia",
                    "Santa Monica",
                    "Westminster",
                    "Redding",
                    "Santa Barbara",
                    "Chico",
                    "Newport Beach",
                    "San Leandro",
                    "San Marcos",
                    "Whittier",
                    "Hawthorne",
                    "Citrus Heights",
                    "Tracy",
                    "Alhambra",
                    "Livermore",
                    "Buena Park",
                    "Menifee",
                    "Hemet",
                    "Lakewood",
                    "Merced",
                    "Chino",
                    "Indio",
                    "Redwood City",
                    "Lake Forest",
                    "Napa",
                    "Tustin",
                    "Bellflower",
                    "Mountain View",
                    "Chino Hills",
                    "Baldwin Park",
                    "Alameda",
                    "Upland",
                    "San Ramon",
                    "Folsom",
                    "Pleasanton",
                    "Union City",
                    "Perris",
                    "Manteca",
                    "Lynwood",
                    "Apple Valley",
                    "Redlands",
                    "Turlock",
                    "Milpitas",
                    "Redondo Beach",
                    "Rancho Cordova",
                    "Yorba Linda",
                    "Palo Alto",
                    "Davis",
                    "Camarillo",
                    "Walnut Creek",
                    "Pittsburg",
                    "South San Francisco",
                    "Yuba City",
                    "San Clemente",
                    "Laguna Niguel",
                    "Pico Rivera",
                    "Montebello",
                    "Lodi",
                    "Madera",
                    "Santa Cruz",
                    "La Habra",
                    "Encinitas",
                    "Monterey Park",
                    "Tulare",
                    "Cupertino",
                    "Gardena",
                    "National City",
                    "Rocklin",
                    "Petaluma",
                    "Huntington Park",
                    "San Rafael",
                    "La Mesa",
                    "Arcadia",
                    "Fountain Valley",
                    "Diamond Bar",
                    "Woodland",
                    "Santee",
                    "Lake Elsinore",
                    "Porterville",
                    "Paramount",
                    "Eastvale",
                    "Rosemead",
                    "Hanford",
                    "Highland",
                    "Brentwood",
                    "Novato",
                    "Colton",
                    "Cathedral City",
                    "Delano",
                    "Yucaipa",
                    "Watsonville",
                    "Placentia",
                    "Glendora",
                    "Gilroy",
                    "Palm Desert",
                    "Cerritos",
                    "West Sacramento",
                    "Aliso Viejo",
                    "Poway",
                    "La Mirada",
                    "Rancho Santa Margarita",
                    "Cypress",
                    "Dublin",
                    "Covina",
                    "Azusa",
                    "Palm Springs",
                    "San Luis Obispo",
                    "Ceres",
                    "San Jacinto",
                    "Lincoln",
                    "Newark",
                    "Lompoc",
                    "El Centro",
                    "Danville",
                    "Bell Gardens",
                    "Coachella",
                    "Rancho Palos Verdes",
                    "San Bruno",
                    "Rohnert Park",
                    "Brea",
                    "La Puente",
                    "Campbell",
                    "San Gabriel",
                    "Beaumont",
                    "Morgan Hill",
                    "Culver City",
                    "Calexico",
                    "Stanton",
                    "La Quinta",
                    "Pacifica",
                    "Montclair",
                    "Oakley",
                    "Monrovia",
                    "Los Banos",
                    "Martinez"
                ],
                "Illinois": [
                    "Chicago",
                    "Aurora",
                    "Rockford",
                    "Joliet",
                    "Naperville",
                    "Springfield",
                    "Peoria",
                    "Elgin",
                    "Waukegan",
                    "Cicero",
                    "Champaign",
                    "Bloomington",
                    "Arlington Heights",
                    "Evanston",
                    "Decatur",
                    "Schaumburg",
                    "Bolingbrook",
                    "Palatine",
                    "Skokie",
                    "Des Plaines",
                    "Orland Park",
                    "Tinley Park",
                    "Oak Lawn",
                    "Berwyn",
                    "Mount Prospect",
                    "Normal",
                    "Wheaton",
                    "Hoffman Estates",
                    "Oak Park",
                    "Downers Grove",
                    "Elmhurst",
                    "Glenview",
                    "DeKalb",
                    "Lombard",
                    "Belleville",
                    "Moline",
                    "Buffalo Grove",
                    "Bartlett",
                    "Urbana",
                    "Quincy",
                    "Crystal Lake",
                    "Plainfield",
                    "Streamwood",
                    "Carol Stream",
                    "Romeoville",
                    "Rock Island",
                    "Hanover Park",
                    "Carpentersville",
                    "Wheeling",
                    "Park Ridge",
                    "Addison",
                    "Calumet City"
                ],
                "Texas": [
                    "Houston",
                    "San Antonio",
                    "Dallas",
                    "Austin",
                    "Fort Worth",
                    "El Paso",
                    "Arlington",
                    "Corpus Christi",
                    "Plano",
                    "Laredo",
                    "Lubbock",
                    "Garland",
                    "Irving",
                    "Amarillo",
                    "Grand Prairie",
                    "Brownsville",
                    "Pasadena",
                    "McKinney",
                    "Mesquite",
                    "McAllen",
                    "Killeen",
                    "Frisco",
                    "Waco",
                    "Carrollton",
                    "Denton",
                    "Midland",
                    "Abilene",
                    "Beaumont",
                    "Round Rock",
                    "Odessa",
                    "Wichita Falls",
                    "Richardson",
                    "Lewisville",
                    "Tyler",
                    "College Station",
                    "Pearland",
                    "San Angelo",
                    "Allen",
                    "League City",
                    "Sugar Land",
                    "Longview",
                    "Edinburg",
                    "Mission",
                    "Bryan",
                    "Baytown",
                    "Pharr",
                    "Temple",
                    "Missouri City",
                    "Flower Mound",
                    "Harlingen",
                    "North Richland Hills",
                    "Victoria",
                    "Conroe",
                    "New Braunfels",
                    "Mansfield",
                    "Cedar Park",
                    "Rowlett",
                    "Port Arthur",
                    "Euless",
                    "Georgetown",
                    "Pflugerville",
                    "DeSoto",
                    "San Marcos",
                    "Grapevine",
                    "Bedford",
                    "Galveston",
                    "Cedar Hill",
                    "Texas City",
                    "Wylie",
                    "Haltom City",
                    "Keller",
                    "Coppell",
                    "Rockwall",
                    "Huntsville",
                    "Duncanville",
                    "Sherman",
                    "The Colony",
                    "Burleson",
                    "Hurst",
                    "Lancaster",
                    "Texarkana",
                    "Friendswood",
                    "Weslaco"
                ],
                "Pennsylvania": [
                    "Philadelphia",
                    "Pittsburgh",
                    "Allentown",
                    "Erie",
                    "Reading",
                    "Scranton",
                    "Bethlehem",
                    "Lancaster",
                    "Harrisburg",
                    "Altoona",
                    "York",
                    "State College",
                    "Wilkes-Barre"
                ],
                "Arizona": [
                    "Phoenix",
                    "Tucson",
                    "Mesa",
                    "Chandler",
                    "Glendale",
                    "Scottsdale",
                    "Gilbert",
                    "Tempe",
                    "Peoria",
                    "Surprise",
                    "Yuma",
                    "Avondale",
                    "Goodyear",
                    "Flagstaff",
                    "Buckeye",
                    "Lake Havasu City",
                    "Casa Grande",
                    "Sierra Vista",
                    "Maricopa",
                    "Oro Valley",
                    "Prescott",
                    "Bullhead City",
                    "Prescott Valley",
                    "Marana",
                    "Apache Junction"
                ],
                "Florida": [
                    "Jacksonville",
                    "Miami",
                    "Tampa",
                    "Orlando",
                    "St. Petersburg",
                    "Hialeah",
                    "Tallahassee",
                    "Fort Lauderdale",
                    "Port St. Lucie",
                    "Cape Coral",
                    "Pembroke Pines",
                    "Hollywood",
                    "Miramar",
                    "Gainesville",
                    "Coral Springs",
                    "Miami Gardens",
                    "Clearwater",
                    "Palm Bay",
                    "Pompano Beach",
                    "West Palm Beach",
                    "Lakeland",
                    "Davie",
                    "Miami Beach",
                    "Sunrise",
                    "Plantation",
                    "Boca Raton",
                    "Deltona",
                    "Largo",
                    "Deerfield Beach",
                    "Palm Coast",
                    "Melbourne",
                    "Boynton Beach",
                    "Lauderhill",
                    "Weston",
                    "Fort Myers",
                    "Kissimmee",
                    "Homestead",
                    "Tamarac",
                    "Delray Beach",
                    "Daytona Beach",
                    "North Miami",
                    "Wellington",
                    "North Port",
                    "Jupiter",
                    "Ocala",
                    "Port Orange",
                    "Margate",
                    "Coconut Creek",
                    "Sanford",
                    "Sarasota",
                    "Pensacola",
                    "Bradenton",
                    "Palm Beach Gardens",
                    "Pinellas Park",
                    "Coral Gables",
                    "Doral",
                    "Bonita Springs",
                    "Apopka",
                    "Titusville",
                    "North Miami Beach",
                    "Oakland Park",
                    "Fort Pierce",
                    "North Lauderdale",
                    "Cutler Bay",
                    "Altamonte Springs",
                    "St. Cloud",
                    "Greenacres",
                    "Ormond Beach",
                    "Ocoee",
                    "Hallandale Beach",
                    "Winter Garden",
                    "Aventura"
                ],
                "Indiana": [
                    "Indianapolis",
                    "Fort Wayne",
                    "Evansville",
                    "South Bend",
                    "Carmel",
                    "Bloomington",
                    "Fishers",
                    "Hammond",
                    "Gary",
                    "Muncie",
                    "Lafayette",
                    "Terre Haute",
                    "Kokomo",
                    "Anderson",
                    "Noblesville",
                    "Greenwood",
                    "Elkhart",
                    "Mishawaka",
                    "Lawrence",
                    "Jeffersonville",
                    "Columbus",
                    "Portage"
                ],
                "Ohio": [
                    "Columbus",
                    "Cleveland",
                    "Cincinnati",
                    "Toledo",
                    "Akron",
                    "Dayton",
                    "Parma",
                    "Canton",
                    "Youngstown",
                    "Lorain",
                    "Hamilton",
                    "Springfield",
                    "Kettering",
                    "Elyria",
                    "Lakewood",
                    "Cuyahoga Falls",
                    "Middletown",
                    "Euclid",
                    "Newark",
                    "Mansfield",
                    "Mentor",
                    "Beavercreek",
                    "Cleveland Heights",
                    "Strongsville",
                    "Dublin",
                    "Fairfield",
                    "Findlay",
                    "Warren",
                    "Lancaster",
                    "Lima",
                    "Huber Heights",
                    "Westerville",
                    "Marion",
                    "Grove City"
                ],
                "North Carolina": [
                    "Charlotte",
                    "Raleigh",
                    "Greensboro",
                    "Durham",
                    "Winston-Salem",
                    "Fayetteville",
                    "Cary",
                    "Wilmington",
                    "High Point",
                    "Greenville",
                    "Asheville",
                    "Concord",
                    "Gastonia",
                    "Jacksonville",
                    "Chapel Hill",
                    "Rocky Mount",
                    "Burlington",
                    "Wilson",
                    "Huntersville",
                    "Kannapolis",
                    "Apex",
                    "Hickory",
                    "Goldsboro"
                ],
                "Michigan": [
                    "Detroit",
                    "Grand Rapids",
                    "Warren",
                    "Sterling Heights",
                    "Ann Arbor",
                    "Lansing",
                    "Flint",
                    "Dearborn",
                    "Livonia",
                    "Westland",
                    "Troy",
                    "Farmington Hills",
                    "Kalamazoo",
                    "Wyoming",
                    "Southfield",
                    "Rochester Hills",
                    "Taylor",
                    "Pontiac",
                    "St. Clair Shores",
                    "Royal Oak",
                    "Novi",
                    "Dearborn Heights",
                    "Battle Creek",
                    "Saginaw",
                    "Kentwood",
                    "East Lansing",
                    "Roseville",
                    "Portage",
                    "Midland",
                    "Lincoln Park",
                    "Muskegon"
                ],
                "Tennessee": [
                    "Memphis",
                    "Nashville-Davidson",
                    "Knoxville",
                    "Chattanooga",
                    "Clarksville",
                    "Murfreesboro",
                    "Jackson",
                    "Franklin",
                    "Johnson City",
                    "Bartlett",
                    "Hendersonville",
                    "Kingsport",
                    "Collierville",
                    "Cleveland",
                    "Smyrna",
                    "Germantown",
                    "Brentwood"
                ],
                "Massachusetts": [
                    "Boston",
                    "Worcester",
                    "Springfield",
                    "Lowell",
                    "Cambridge",
                    "New Bedford",
                    "Brockton",
                    "Quincy",
                    "Lynn",
                    "Fall River",
                    "Newton",
                    "Lawrence",
                    "Somerville",
                    "Waltham",
                    "Haverhill",
                    "Malden",
                    "Medford",
                    "Taunton",
                    "Chicopee",
                    "Weymouth Town",
                    "Revere",
                    "Peabody",
                    "Methuen",
                    "Barnstable Town",
                    "Pittsfield",
                    "Attleboro",
                    "Everett",
                    "Salem",
                    "Westfield",
                    "Leominster",
                    "Fitchburg",
                    "Beverly",
                    "Holyoke",
                    "Marlborough",
                    "Woburn",
                    "Chelsea"
                ],
                "Washington": [
                    "Seattle",
                    "Spokane",
                    "Tacoma",
                    "Vancouver",
                    "Bellevue",
                    "Kent",
                    "Everett",
                    "Renton",
                    "Yakima",
                    "Federal Way",
                    "Spokane Valley",
                    "Bellingham",
                    "Kennewick",
                    "Auburn",
                    "Pasco",
                    "Marysville",
                    "Lakewood",
                    "Redmond",
                    "Shoreline",
                    "Richland",
                    "Kirkland",
                    "Burien",
                    "Sammamish",
                    "Olympia",
                    "Lacey",
                    "Edmonds",
                    "Bremerton",
                    "Puyallup"
                ],
                "Colorado": [
                    "Denver",
                    "Colorado Springs",
                    "Aurora",
                    "Fort Collins",
                    "Lakewood",
                    "Thornton",
                    "Arvada",
                    "Westminster",
                    "Pueblo",
                    "Centennial",
                    "Boulder",
                    "Greeley",
                    "Longmont",
                    "Loveland",
                    "Grand Junction",
                    "Broomfield",
                    "Castle Rock",
                    "Commerce City",
                    "Parker",
                    "Littleton",
                    "Northglenn"
                ],
                "District of Columbia": [
                    "Washington"
                ],
                "Maryland": [
                    "Baltimore",
                    "Frederick",
                    "Rockville",
                    "Gaithersburg",
                    "Bowie",
                    "Hagerstown",
                    "Annapolis"
                ],
                "Kentucky": [
                    "Louisville/Jefferson County",
                    "Lexington-Fayette",
                    "Bowling Green",
                    "Owensboro",
                    "Covington"
                ],
                "Oregon": [
                    "Portland",
                    "Eugene",
                    "Salem",
                    "Gresham",
                    "Hillsboro",
                    "Beaverton",
                    "Bend",
                    "Medford",
                    "Springfield",
                    "Corvallis",
                    "Albany",
                    "Tigard",
                    "Lake Oswego",
                    "Keizer"
                ],
                "Oklahoma": [
                    "Oklahoma City",
                    "Tulsa",
                    "Norman",
                    "Broken Arrow",
                    "Lawton",
                    "Edmond",
                    "Moore",
                    "Midwest City",
                    "Enid",
                    "Stillwater",
                    "Muskogee"
                ],
                "Wisconsin": [
                    "Milwaukee",
                    "Madison",
                    "Green Bay",
                    "Kenosha",
                    "Racine",
                    "Appleton",
                    "Waukesha",
                    "Eau Claire",
                    "Oshkosh",
                    "Janesville",
                    "West Allis",
                    "La Crosse",
                    "Sheboygan",
                    "Wauwatosa",
                    "Fond du Lac",
                    "New Berlin",
                    "Wausau",
                    "Brookfield",
                    "Greenfield",
                    "Beloit"
                ],
                "Nevada": [
                    "Las Vegas",
                    "Henderson",
                    "Reno",
                    "North Las Vegas",
                    "Sparks",
                    "Carson City"
                ],
                "New Mexico": [
                    "Albuquerque",
                    "Las Cruces",
                    "Rio Rancho",
                    "Santa Fe",
                    "Roswell",
                    "Farmington",
                    "Clovis"
                ],
                "Missouri": [
                    "Kansas City",
                    "St. Louis",
                    "Springfield",
                    "Independence",
                    "Columbia",
                    "Lee's Summit",
                    "O'Fallon",
                    "St. Joseph",
                    "St. Charles",
                    "St. Peters",
                    "Blue Springs",
                    "Florissant",
                    "Joplin",
                    "Chesterfield",
                    "Jefferson City",
                    "Cape Girardeau"
                ],
                "Virginia": [
                    "Virginia Beach",
                    "Norfolk",
                    "Chesapeake",
                    "Richmond",
                    "Newport News",
                    "Alexandria",
                    "Hampton",
                    "Roanoke",
                    "Portsmouth",
                    "Suffolk",
                    "Lynchburg",
                    "Harrisonburg",
                    "Leesburg",
                    "Charlottesville",
                    "Danville",
                    "Blacksburg",
                    "Manassas"
                ],
                "Georgia": [
                    "Atlanta",
                    "Columbus",
                    "Augusta-Richmond County",
                    "Savannah",
                    "Athens-Clarke County",
                    "Sandy Springs",
                    "Roswell",
                    "Macon",
                    "Johns Creek",
                    "Albany",
                    "Warner Robins",
                    "Alpharetta",
                    "Marietta",
                    "Valdosta",
                    "Smyrna",
                    "Dunwoody"
                ],
                "Nebraska": [
                    "Omaha",
                    "Lincoln",
                    "Bellevue",
                    "Grand Island"
                ],
                "Minnesota": [
                    "Minneapolis",
                    "St. Paul",
                    "Rochester",
                    "Duluth",
                    "Bloomington",
                    "Brooklyn Park",
                    "Plymouth",
                    "St. Cloud",
                    "Eagan",
                    "Woodbury",
                    "Maple Grove",
                    "Eden Prairie",
                    "Coon Rapids",
                    "Burnsville",
                    "Blaine",
                    "Lakeville",
                    "Minnetonka",
                    "Apple Valley",
                    "Edina",
                    "St. Louis Park",
                    "Mankato",
                    "Maplewood",
                    "Moorhead",
                    "Shakopee"
                ],
                "Kansas": [
                    "Wichita",
                    "Overland Park",
                    "Kansas City",
                    "Olathe",
                    "Topeka",
                    "Lawrence",
                    "Shawnee",
                    "Manhattan",
                    "Lenexa",
                    "Salina",
                    "Hutchinson"
                ],
                "Louisiana": [
                    "New Orleans",
                    "Baton Rouge",
                    "Shreveport",
                    "Lafayette",
                    "Lake Charles",
                    "Kenner",
                    "Bossier City",
                    "Monroe",
                    "Alexandria"
                ],
                "Hawaii": [
                    "Honolulu"
                ],
                "Alaska": [
                    "Anchorage"
                ],
                "New Jersey": [
                    "Newark",
                    "Jersey City",
                    "Paterson",
                    "Elizabeth",
                    "Clifton",
                    "Trenton",
                    "Camden",
                    "Passaic",
                    "Union City",
                    "Bayonne",
                    "East Orange",
                    "Vineland",
                    "New Brunswick",
                    "Hoboken",
                    "Perth Amboy",
                    "West New York",
                    "Plainfield",
                    "Hackensack",
                    "Sayreville",
                    "Kearny",
                    "Linden",
                    "Atlantic City"
                ],
                "Idaho": [
                    "Boise City",
                    "Nampa",
                    "Meridian",
                    "Idaho Falls",
                    "Pocatello",
                    "Caldwell",
                    "Coeur d'Alene",
                    "Twin Falls"
                ],
                "Alabama": [
                    "Birmingham",
                    "Montgomery",
                    "Mobile",
                    "Huntsville",
                    "Tuscaloosa",
                    "Hoover",
                    "Dothan",
                    "Auburn",
                    "Decatur",
                    "Madison",
                    "Florence",
                    "Gadsden"
                ],
                "Iowa": [
                    "Des Moines",
                    "Cedar Rapids",
                    "Davenport",
                    "Sioux City",
                    "Iowa City",
                    "Waterloo",
                    "Council Bluffs",
                    "Ames",
                    "West Des Moines",
                    "Dubuque",
                    "Ankeny",
                    "Urbandale",
                    "Cedar Falls"
                ],
                "Arkansas": [
                    "Little Rock",
                    "Fort Smith",
                    "Fayetteville",
                    "Springdale",
                    "Jonesboro",
                    "North Little Rock",
                    "Conway",
                    "Rogers",
                    "Pine Bluff",
                    "Bentonville"
                ],
                "Utah": [
                    "Salt Lake City",
                    "West Valley City",
                    "Provo",
                    "West Jordan",
                    "Orem",
                    "Sandy",
                    "Ogden",
                    "St. George",
                    "Layton",
                    "Taylorsville",
                    "South Jordan",
                    "Lehi",
                    "Logan",
                    "Murray",
                    "Draper",
                    "Bountiful",
                    "Riverton",
                    "Roy"
                ],
                "Rhode Island": [
                    "Providence",
                    "Warwick",
                    "Cranston",
                    "Pawtucket",
                    "East Providence",
                    "Woonsocket"
                ],
                "Mississippi": [
                    "Jackson",
                    "Gulfport",
                    "Southaven",
                    "Hattiesburg",
                    "Biloxi",
                    "Meridian"
                ],
                "South Dakota": [
                    "Sioux Falls",
                    "Rapid City"
                ],
                "Connecticut": [
                    "Bridgeport",
                    "New Haven",
                    "Stamford",
                    "Hartford",
                    "Waterbury",
                    "Norwalk",
                    "Danbury",
                    "New Britain",
                    "Meriden",
                    "Bristol",
                    "West Haven",
                    "Milford",
                    "Middletown",
                    "Norwich",
                    "Shelton"
                ],
                "South Carolina": [
                    "Columbia",
                    "Charleston",
                    "North Charleston",
                    "Mount Pleasant",
                    "Rock Hill",
                    "Greenville",
                    "Summerville",
                    "Sumter",
                    "Goose Creek",
                    "Hilton Head Island",
                    "Florence",
                    "Spartanburg"
                ],
                "New Hampshire": [
                    "Manchester",
                    "Nashua",
                    "Concord"
                ],
                "North Dakota": [
                    "Fargo",
                    "Bismarck",
                    "Grand Forks",
                    "Minot"
                ],
                "Montana": [
                    "Billings",
                    "Missoula",
                    "Great Falls",
                    "Bozeman"
                ],
                "Delaware": [
                    "Wilmington",
                    "Dover"
                ],
                "Maine": [
                    "Portland"
                ],
                "Wyoming": [
                    "Cheyenne",
                    "Casper"
                ],
                "West Virginia": [
                    "Charleston",
                    "Huntington"
                ],
                "Vermont": [
                    "Burlington"
                ]
            },
            seen: true,
            activeSmsinput: false,
            //isChecked: false,
        },
        watch: {
            alreadySelectedLeadStates: function (val) {
                // this.$data.alreadySelectedLeadStates = val;
                // console.log(val);
                // Object.keys(yourObject).length === 0

                if (val == 0) {
                    // alert('hi');
                    this.$data.alreadySelectedLeadStates = val;
                    console.log('hi');
                } else {
                    console.log(val);
                    //console.log('hi');
                }
            }
        },
        methods: {
            click: function () {
                this.show = !this.show;
                // this.activeSmsinput = !this.activeSmsinput

            },
            preStateTrue: function () {
                // console.log(this.tasks.description);
                this.preClickstate = true;
                //this.seen = false;
            },
            disallowMovingStatesWindowTrue: function () {
                this.disallowMovingStatesWindowSwitcher = true;
            },
            activeSmsinputMthd: function () {
                this.activeSmsinput = true;
            }

        },

        mounted() {
            if ("@yield('disallowLeadStates')") {
                var arr = "@yield('disallowLeadStates')";
                //console.log(arr);
                //console.log(JSON.parse(arr.replace(/&quot;/g,'"')));

                var selectedStates = JSON.parse(arr.replace(/&quot;/g, '"'));
                //console.log(selectedStates);
                this.$data.alreadySelectedLeadStates = selectedStates;

            }
            if ("@yield('disallowMovingStates')") {
                var arr = "@yield('disallowMovingStates')";
               // console.log(arr);
                //console.log(JSON.parse(arr.replace(/&quot;/g,'"')));

                var selectedStates = JSON.parse(arr.replace(/&quot;/g, '"'));
               // console.log(selectedStates);
                this.$data.alreadySelectedMovingStates = selectedStates;

            }
            if ("@yield('moverState')") {
                var arr = "@yield('moverState')";
                console.log(arr);               
                this.$data.moverStateName = arr;

            }
            if ("@yield('moverCity')") {
                var arr = "@yield('moverCity')";
                console.log(arr);               
                this.$data.moverCityName = arr;

            }

            if ("@yield('show')" == 'true') {
                // alert('hi');
                this.show = true,
                    this.enable = true
            }
            if ("@yield('checkSmsPhn')" == 'checked') {
                alert('hi')
                this.activeSmsinput = true

            }
        },


    });

</script>




@endsection
