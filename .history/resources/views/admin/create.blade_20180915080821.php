    @extends('admin.layouts.app')
    @section('title', 'create Mover')

    @section('goBody')

    <div id="app">
    <h5>Add New Moving Companies</h5>
    <p>To add new movers fill up the form bellow <span class="bg-warning">Once you have created a new movers you need
    to add CHAMPAIGN to send lead to movers.</span>
    </p>
    <hr>
    <h5>Moving CoMpany Information <span class="text-muted">enter company information below</span></h5>
    <hr>
    @include('admin.includes.messages')
    {{--@{{message}}--}}

    <form action="/admin/mover/@yield('editId')" method="post" class="">
    {{csrf_field()}}
    @section('editMethod')
    @show
    <fieldset>
    <div class="form-horizontal">
    <div class="form-group">
        <label class="col-md-3 control-label" for="userName">User Name*</label>
        <div class="col-md-4">
            <input type="text" name="userName" id="title" class="form-control" value="@yield('userName')"
                placeholder="userName" aria-describedby="helpId">
        </div>
        <div class="col-md-5">
            <label class="control-label"><span class="text-info text-muted">Enter Mover User Name</span></label>
        </div>
    </div>
    <hr class="dotted">
    <div class="form-group">
        <label for="password" class="col-md-3 control-label">User Password</label>
        <div class="col-md-4">
            <input type="text" name="password" id="title" class="form-control" value="@yield('password')"
                placeholder="password" aria-describedby="helpId">
        </div>
        <div class="col-md-5">
            <label class="control-label"><span class="text-info text-muted">Enter Mover Password</span></label>
        </div>
    </div>
    <hr class="dotted">
    </div>


    <h5 class="text-success">User More Information <span class="text-muted">enter company information below</span></h5>
    <hr>
    <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="moverName">Movers Company Name <span class="text-muted">Exp. Arrow Moving</span></label>
            <input type="text" name="moverName" id="title" class="form-control" value="@yield('moverName')"
                placeholder="moverName" aria-describedby="helpId">
        </div>
        <div class="form-group">
            <label for="usDotNumber">USDot Number <span class="text-muted">Exp. 17371</span></label>
            <input type="text" name="usDotNumber" id="title" class="form-control" value="@yield('usDotNumber')"
                placeholder="usDotNumber" aria-describedby="helpId">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="ownerName">Movers Owner's Name* <span class="text-muted">Exp. Nicole Ford</span></label>
            <input type="text" name="ownerName" id="title" class="form-control" value="@yield('ownerName')"
                placeholder="ownerName" aria-describedby="helpId">
        </div>
        <div class="form-group">
            <label for="ownerMccNum">Movers MCC Number</label>
            <input type="text" name="ownerMccNum" id="title" class="form-control" value="@yield('ownerMccNum')"
                placeholder="ownerMccNum" aria-describedby="helpId">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="contactName">Movers Contact Name* <span class="text-muted">Exp. John Hashi</span></label>
            <input type="text" name="contactName" id="title" class="form-control" value="@yield('contactName')"
                placeholder="contactName" aria-describedby="helpId">
        </div>
        <div class="form-group">
            <label for="otherLcnInfo">Movers Other licence Info</label>
            <input type="text" name="otherLcnInfo" id="title" class="form-control" value="@yield('otherLcnInfo')"
                placeholder="otherLcnInfo" aria-describedby="helpId">
        </div>
    </div>


    </div>
    <hr class="dotted">

    <h5 class="text-success">Moving Partner Info</h5>
    <hr>
    <div class="form-horizontal">
    <div class="">
        <div class="form-group">
            <label for="gnrlEmail" class="col-md-3 control-label">General Emil*</label>
            <div class="col-md-4">
                <input type="text" name="gnrlEmail" id="title" class="form-control" value="@yield('gnrlEmail')"
                    placeholder="gnrlEmail" aria-describedby="helpId">
            </div>
            <div class="col-md-5">
                <label class="control-label"><span class="text-info text-muted">Would be use for
                        communication</span></label>
            </div>
        </div>
        <hr class="dotted">
        <div class="form-group">
            <label for="leadCollectEmail" class="col-md-3 control-label">Lead Recevining Email*</label>
            <div class="col-md-4">
                <input type="text" name="leadCollectEmail" id="title" class="form-control" value="@yield('leadCollectEmail')"
                    placeholder="leadCollectEmail" aria-describedby="helpId">
            </div>
            <div class="col-md-5">
                <label class="control-label"><span class="text-info text-muted">This email will receive
                        leads</span></label>
            </div>
        </div>
        <hr class="dotted">
    </div>
    </div>
    <h5 class="text-success">Lead Receiving Settings</h5>
    <hr>
    <div class="row">
    <div class="col-md-12">

        <div class="checkbox-custom checkbox-inline">
            <input id="enable" type="checkbox" v-model="enable" name="chkSendList" value="checked" @click="click"
                @yield('chkSendList')>
            <label for="chkSendList">Send List</label>
        </div>


        <span v-if="show" {{-- @yield('show') --}}>
            <div class="radio-custom radio-inline">
                <input type="radio" :disabled="!enable" name="chkFrmtHtml" value="1" @yield('chkFrmtHtml')>
                <label for="chkFrmtHtml">( HTML <span class="text-danger">or</span></label>

            </div>
            <div class="radio-custom radio-inline">
                <input type="radio" :disabled="!enable" name="chkFrmtHtml" value="0" @yield('plain')>
                <label for="chkFrmtHtml">Plain ) via Email. <span class="text-muted bg-warning">Will send
                        Leads Data only to email Address</span></label>
            </div>
        </span>
        <div class="moveritem">

            <ol>

                {{-- <mover-item v-for="item in options" v-bind:key="options.id" v-bind:options="item"></mover-item>--}}
                {{-- <mover-item v-for="item in options" v-bind:options="item" v-bind:key="item.id"></mover-item>--}}
            </ol>
        </div>


    </div>
    </div>
    <hr class="dotted">

    <h5 class="text-success">Send SMS to Mover Cell Phone <span class="text-muted">if checked then a SMS will
        send Movers cell Number</span>
    </h5>
    <hr>
    <div class="form-horizontal left">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <div class="checkbox-custom checkbox-inline">
                    <input name="checkSmsPhn" v-model="activeSmsinput" type="checkbox" value="checked"
                        @yield('checkSmsPhn')>
                    <label for="checkSmsPhn">Send SMS To Movers Cell Phone</label>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input type="text" name="smsNumber" id="title" class="form-control" :disabled="!activeSmsinput"
                    value="@yield('smsNumber')" placeholder="smsNumber" aria-describedby="helpId">

            </div>
        </div>
        <div class="col-md-6">
            <p class="control-label"><span class="text-info">Exp. 3472622404. Just Plain Number. NO space,
                    no - or +, no (or), no leading 0 or 1.</span>
            </p>
        </div>
        <hr class="dotted">
    </div>

    </div>


    <h5 class="text-success">Send To GRANOT <span class="text-muted">send list to via granot</span></h5>
    <hr class="dotted">
    <div class="row">
    <div class="col-md-12">
        <div class="checkbox-custom checkbox-inline">
            <input name="chkGranot" type="checkbox" value="checked" @yield('chkGranot')>
            <label for="chkGranot">Send Lead Data To Customer Granot only ( Note: Not Car Data )</label>
        </div>
    </div>

    </div>
    <hr class="dotted">

    <h5 class="text-success">Lead Pricing <span class="text-muted">Enter Long and Local Pricing Below</span>
    </h5>
    <hr>
    <div class="form-horizontal">
    <div class="form-group">
        <label for="longLeadPrice" class="col-md-3 control-label">Long Lead Price*</label>
        <div class="col-md-2">
            <input type="text" name="longLeadPrice" class="form-control" value="@yield('longLeadPrice')"
                placeholder="longLeadPrice" aria-describedby="helpId">
        </div>
        <div class="col-md-7">
            <label class="control-label"><span class="text-info text-muted">how much to change for long
                    leads</span></label>
        </div>
    </div>
    <hr class="dotted">
    <div class="form-group">
        <label for="localLeadPrice" class="col-md-3 control-label">Local Lead Price*</label>
        <div class="col-md-2">
            <input type="text" name="localLeadPrice" class="form-control" value="@yield('localLeadPrice')"
                placeholder="localLeadPrice" aria-describedby="helpId">
        </div>
        <div class="col-md-7">
            <label class="control-label"><span class="text-info text-muted">how much to change for long
                    leads</span></label>
        </div>
    </div>
    <hr class="dotted">
    </div>

    <h5 class="text-success">Lead Per Hour <span class="text-muted">Enter how many leads movers wants per hour
        basis</span>
    <span class="bg-warning text-muted">
        enter 0 for unlimited
    </span></h5>
    <hr>
    <div class="form-horizontal">
    <div class="form-group">
        <label for="leadPerHour" class="col-md-3 control-label">How Many leads Per Hour*</label>
        <div class="col-md-2">
            <input type="text" name="leadPerHour" class="form-control" value="@yield('leadPerHour')"
                placeholder="leadPerHour" aria-describedby="helpId">
        </div>
        <div class="col-md-7">
            <label class="control-label"><span class="text-info text-muted">put 0 to unlimited</span></label>
        </div>
    </div>
    <hr class="dotted">
    </div>
    {{--newly inserted--}}
    <h5 class="text-success">Want Car Leads Data <span class="text-muted bg-warning">check it ON if you want
        send car leads data so client might get 2 email 1 for normal move and 1 for car move</span>
    </h5>

    <hr>
    <div class="form-horizontal">
    <div class="row">
        <div class="col-md-12">
            <div class="checkbox-custom checkbox-inline">
                <input name="chkcarleadsEmail" type="checkbox" value="checked" @yield('chkcarleadsEmail')>
                <label for="chkcarleadsEmail">Send Car Leads Data to Customers Email Address
                    Only</label>
            </div>
        </div>

    </div>
    <hr class="dotted">
    <div class="row">
        <div class="col-md-12">
            <div class="checkbox-custom checkbox-inline">
                <input name="chkcarleadsGranot" type="checkbox" value="checked" @yield('chkcarleadsGranot')>
                <label for="chkcarleadsGranot">Send Car Leads Data to Customer Granot <span class="bg-warning text-muted">(
                        Car Data Only Not Regular Lead. Lead receiving email will be use for Granot )</span></label>
            </div>
        </div>

    </div>
    <hr class="dotted">
    </div>


    <h5 class="text-success">Car Lead Pricing <span class="text-muted">Enter Long and Local Pricing Below</span>
    </h5>
    <hr>
    <div class="form-horizontal">
    <div class="form-group">
        <label for="longCarLeadPrice" class="col-md-3 control-label">Long Car Lead Price*</label>
        <div class="col-md-2">
            <input type="text" name="longCarLeadPrice" class="form-control" value="@yield('longCarLeadPrice')"
                placeholder="longCarLeadPrice" aria-describedby="helpId">
        </div>
        <div class="col-md-7">
            <label class="control-label"><span class="text-info text-muted">how much to change for long car
                    leads</span></label>
        </div>
    </div>
    <hr class="dotted">
    <div class="form-group">
        <label for="localCarLeadPrice" class="col-md-3 control-label">Local Car Lead Price*</label>
        <div class="col-md-2">
            <input type="text" name="localCarLeadPrice" class="form-control" value="@yield('localCarLeadPrice')"
                placeholder="localCarLeadPrice" aria-describedby="helpId">
        </div>
        <div class="col-md-7">
            <label class="control-label"><span class="text-info text-muted">how much to change for long car
                    leads</span></label>
        </div>
    </div>
    <hr class="dotted">
    </div>

    <h5 class="text-success">Prohabitted States For Leads</h5>
    <p class="text-muted">If a mover does not want any leads from a specific state or does not want to move
    to a specific state then mention it here. it is useful for zip and surrounding miles champaign
    settings.</p>
    <hr>
    <div class="form-horizontal">
    <div class="form-group">
        <label for="disallowLeadStates" class="col-md-3 control-label">Do Not Want Lead From These
            States</label>
        <div class="col-md-4">
            <span v-for="item in statesName">@{{item}}, </span>

            <input type="text" name="disallowLeadStates" class="form-control" v-model="alreadyDisallowLeadStates"
                placeholder="Add States To Disallow" {{--value="--}}
                {{--@foreach($stateSelected as $value){{$value}} , @endforeach--}}{{--"--}}
                aria-describedby="helpId">

            {{--
            @{{ alreadyDisallowLeadStates }}
            <ul>
                <li v-for="item in alreadyDisallowLeadStates">
                    @{{ item }}
                </li>
            </ul>
            --}}

        </div>
        <div class="col-md-5">
            <div v-show="preClickstate" class="disallowStates">
                <div class="stateListContainerWrapper" style="position: relative">
                    <div class="btnGroup" style="position: absolute">
                        <button type="button" class="btn btn-info" v-on:click="preClickstate = false"><b>Ok
                                Done!</b>
                        </button>
                        {{--<span class="pull-right" v-on:click="this.$data.preClickstate = false">&times;</span>--}}

                    </div>
                    <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true" v-on:click="preClickstate = false">&times;</span>
                    </button>
                    <div class="stateListContainer">


                        <ul class="list-group" style="columns: 3;padding: 0px 0 15px 15px;">
                            <li class="" v-for="item in states">
                                <div class="checkbox-custom checkbox-inline">
                                    <input type="checkbox" v-model="alreadyDisallowLeadStates" name="disallowLeadStates[]"
                                        :value="item.abbreviation">
                                    <label :for="item.abbreviation"> @{{ item.name }}</label>
                                </div>

                            </li>

                        </ul>
                    </div>
                </div>



            </div>


            <button type="button" class="btn btn-default mr5 mb10 btn-warning" @click="preStateTrue">
                <i class="fa fa-plus"></i> States
            </button>
        </div>


    </div>
    <hr class="dotted">
    <div class="form-group">
        <label for="disallowMovingStates" class="col-md-3 control-label">Do Not Want Leads Moving TO
            These States</label>
        <div class="col-md-4">
            <input type="text" name="disallowMovingStates" class="form-control" v-model="alreadyDisallowMovingStates"
                {{-- value="@yield('disallowMovingStates')" --}} placeholder="disallowMovingStates"
                aria-describedby="helpId">
        </div>
        <div class="col-md-5">
<div v-show="disallowMovingStatesWindowSwitcher" class="">
<div class="stateListContainerWrapper" style="position: relative">
<div class="btnGroup" style="position: absolute">
    <button type="button" class="btn btn-info" v-on:click="disallowMovingStatesWindowSwitcher = false"><b>Ok
            Done!</b>
    </button>
    {{--<span class="pull-right" v-on:click="this.$data.preClickstate = false">&times;</span>--}}

</div>
<button type="button" class="close" aria-label="Close">zipCode
    <span aria-hidden="true" v-on:click="disallowMovingStatesWindowSwitcher = false">&times;</span>
</button>
<div class="stateListContainer">


    <ul class="list-group" style="columns: 3;padding: 0px 0 15px 15px;">
        <li class="" v-for="item in states">
            <div class="checkbox-custom checkbox-inline">
                <input type="checkbox" v-model="alreadyDisallowMovingStates" name="disallowMovingStates[]"
                    :value="item.abbreviation">
                <label :for="item.abbreviation"> @{{ item.name }}</label>
            </div>

        </li>

    </ul>
</div>
</div>



</div>


<button type="button" class="btn btn-default mr5 mb10 btn-warning" @click="disallowMovingStatesWindowTrue">
<i class="fa fa-plus"></i> States
</button>

        </div>

    </div>
    <hr class="dotted">

    </div>

    {{--newly inserted--}}
    <h5 class="text-success">Moving Company Address Informarion <span class="text-muted">enter company address
        information below</span>
    </h5>
    <hr>
<div class="row">
    <div class="col-md-4">
            <div class="form-group">
                    <label for="moverState">Mover State</label>        
                    <select class="form-control" name="moverState" v-model="moverStateName"  @change="onStateChange()" @click="selectStateTrue = true">
                        <optgroup label="Prev. @yield('moverState')">
                            <option value="selectState">Select State</option>
                            <option class="" v-for="(value, key, index) in myStates" :value="key">@{{
                                value.name}}</option>
        
                        </optgroup>
                    </select>
        
                </div>
              {{--   @{{myStates[moverStateName]}} --}}
                {{-- Code in your template file, e.g., view.blade.php --}}

               {{--  @foreach ($items as $item)
                <li vue-this>moverCityName-></li>
              @endforeach --}}

                <div class="form-group">
                        <label for="moverCity">Mover City</label>   
                        <select class="form-control" name="moverCity" v-model="moverCityDefautSelected" :disabled="!selectStateTrue" @click="selectZipTrue = true">
                            <optgroup label="Prev. @yield('moverCity')">                                
                                <option value="SelectCity">Select City</option>
                                <option value="@yield('moverCity')">@yield('moverCity')</option>
                                
                                <option class="" v-for="(value, key, index) in moverCityNameItter">@{{value}}</option>
            
                            </optgroup>
                        </select>
                        
            
                 </div>
                
               

    </div>
    <div class="col-md-4">
       
            <div class="form-group">
                    <label for="zipCode">Zip Code</label>
                    {{-- <input type="text" name="zipCode" id="title" class="form-control" value="@yield('zipCode')"
                        placeholder="zipCode" aria-describedby="helpId">--}}
                    <select class="form-control" name="zipCode" v-model="moverZipcode" :disabled="!selectZipTrue">
                        <optgroup label="Prev. @yield('zipCode')">
                            <option value="selectZipCode">Select Zip</option>
                            <option value="@yield('zipCode')">@yield('zipCode')</option>
                            <option class="" v-for="(value, key, index) in moverCityName[moverCityDefautSelected]" :value="value">@{{value}}</option>
        
                        </optgroup>
                    </select>
            </div>
            <div class="form-group">
                    <label for="moverPhoneNumber">Mover Phone Number</label>
                    <input type="text" name="moverPhoneNumber" id="title" class="form-control" value="@yield('moverPhoneNumber')"
                        placeholder="moverPhoneNumber" aria-describedby="helpId">
            </div>
       
    </div>
    <div class="col-md-4">
        
            
            

        <div class="form-group">
            <label for="moverFaxNumber">Mover Fax Number</label>
            <input type="text" name="moverFaxNumber" id="title" class="form-control" value="@yield('moverFaxNumber')"
                placeholder="moverFaxNumber">
        </div>
        <div class="form-group">
                        <label for="moverAdd">mover Address</label>
                        <input type="text" name="moverAdd" id="title" class="form-control" value="@yield('moverAdd')"
                            placeholder="moverAdd" aria-describedby="helpId">
        </div>
    </div>


</div>
    <hr class="dotted">

    {{--newly inserted--}}
    <h5 class="text-success">Lead Receiving Settings</h5>
    <hr>
<div class="row">
    <div class="col-md-12">
        <div class="checkbox-custom checkbox-inline">
            <input type="checkbox" name="chkPaypal" value="checked" @yield('chkPaypal')>
            <label for="chkPaypal">Paying Via Paypal</label>
        </div>

        <div class="checkbox-custom checkbox-inline">
            <input type="checkbox" name="chkCc" value="checked" @yield('chkCc')>
            <label for="chkCc">Paying Via CC</label>
        </div>


    </div>
</div>
    <hr class="dotted">

    {{--newly inserted--}}
    <h5 class="text-success">Mover commpany Credit Card Informarion</h5>
    <hr>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="cardHolderName">Card Holder Name</label>
            <input type="text" name="cardHolderName" id="title" class="form-control" value="@yield('cardHolderName')"
                placeholder="cardHolderName" aria-describedby="helpId">
        </div>
        <div class="form-group">
            <label for="cardExpMonth">Expiration Month</label>
            <select class="form-control" name="cardExpMonth">
                <optgroup label="Pacific Time Zone">
                    <option value="CA">cardExpMonth</option>
                    <option value="NV">Nevada</option>
                    <option value="OR">Oregon</option>
                    <option value="WA">Washington</option>
                </optgroup>
            </select>
        </div>

    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="ccNumber">CC Number</label>
            <input type="text" name="ccNumber" id="title" class="form-control" value="@yield('ccNumber')"
                placeholder="ccNumber" aria-describedby="helpId">
        </div>

        <div class="form-group">
            <label for="cardExpYear">Expiration Year</label>
            <select class="form-control" name="cardExpYear" id="dob">               
                    <option value="0">Year:</option>
                    <option v-for="year in years" :value="year">@{{ year }}</option>
              
            </select>
        </div>
    </div>
    <div class="col-md-4">
        {{-- @{{$firstTelvDigit}}--}}
        <div class="form-group">
            <label for="cwNumber">CW Number</label>
            <input type="text" name="cwNumber" id="title" class="form-control" value="@yield('cwNumber')"
                placeholder="cwNumber" aria-describedby="helpId">
        </div>

    </div>


</div>
    <hr class="dotted">
    <h5 class="text-success">Enter details/remarks about customer <span class="text-muted">html enable help
        text. be aware about html tag completion </span>
    </h5>
    <hr>
    <textarea id="summernote" name="detailsCustomer" class="note-codable" placeholder="editor" aria-describedby="helpId"> @yield('detailsCustomer')</textarea>

    <br>
    <button type="submit" class="btn btn-primary">Submit</button>

    </fieldset>


    </form>





    </div>

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
    <script>
    // window.Vue = require('vue')
    //var apiURL = 'https://raw.githubusercontent.com/Fyresite/US-City-State-Zip-Master-JSON/master/states.min.json';
    var apiURL = 'http://localhost:8000/admin/html/test/test.json';

    //http://localhost/admin/html/test/test.json
    //var apiURL = '../assets/data.json';
    //import json from './Views/jsonData/StateCityZip.json'
    var vm = new Vue({
    el: '#app',
    
        data: {
            message: 'Hello Sir!',
            userData: [],
            enable: false,
            show: false,
            preClickstate: false,
            disallowMovingStatesWindowSwitcher: false,
            state: true,
            selected: [], // Must be an array reference!
            myString: [],
            alreadyDisallowLeadStates: [],
            alreadyDisallowMovingStates: [],
            watchingSomething: [],
            statesName: [],
            moverStateName: 'selectState',
            moverCityDefautSelected: 'SelectCity',
            moverZipcode: 'selectZipCode',
            month: ["January","February","March","April","May","June","July",
            "August","September","October","November","December"],
            
            states: [{
                        "name": "Alabama",
                        "abbreviation": "AL"
                    },
                    {
                        "name": "Alaska",
                        "abbreviation": "AK"
                    },
                    {
                        "name": "American Samoa",
                        "abbreviation": "AS"
                    },
                    {
                        "name": "Arizona",
                        "abbreviation": "AZ"
                    },
                    {
                        "name": "Arkansas",
                        "abbreviation": "AR"
                    },
                    {
                        "name": "California",
                        "abbreviation": "CA"
                    },
                    {
                        "name": "Colorado",
                        "abbreviation": "CO"
                    },
                    {
                        "name": "Connecticut",
                        "abbreviation": "CT"
                    },
                    {
                        "name": "Delaware",
                        "abbreviation": "DE"
                    },
                    {
                        "name": "District Of Columbia",
                        "abbreviation": "DC"
                    },
                    {
                        "name": "Federated States Of Micronesia",
                        "abbreviation": "FM"
                    },
                    {
                        "name": "Florida",
                        "abbreviation": "FL"
                    },
                    {
                        "name": "Georgia",
                        "abbreviation": "GA"
                    },
                    {
                        "name": "Guam",
                        "abbreviation": "GU"
                    },
                    {
                        "name": "Hawaii",
                        "abbreviation": "HI"
                    },
                    {
                        "name": "Idaho",
                        "abbreviation": "ID"
                    },
                    {
                        "name": "Illinois",
                        "abbreviation": "IL"
                    },
                    {
                        "name": "Indiana",
                        "abbreviation": "IN"
                    },
                    {
                        "name": "Iowa",
                        "abbreviation": "IA"
                    },
                    {
                        "name": "Kansas",
                        "abbreviation": "KS"
                    },
                    {
                        "name": "Kentucky",
                        "abbreviation": "KY"
                    },
                    {
                        "name": "Louisiana",
                        "abbreviation": "LA"
                    },
                    {
                        "name": "Maine",
                        "abbreviation": "ME"
                    },
                    {
                        "name": "Marshall Islands",
                        "abbreviation": "MH"
                    },
                    {
                        "name": "Maryland",
                        "abbreviation": "MD"
                    },
                    {
                        "name": "Massachusetts",
                        "abbreviation": "MA"
                    },
                    {
                        "name": "Michigan",
                        "abbreviation": "MI"
                    },
                    {
                        "name": "Minnesota",
                        "abbreviation": "MN"
                    },
                    {
                        "name": "Mississippi",
                        "abbreviation": "MS"
                    },
                    {
                        "name": "Missouri",
                        "abbreviation": "MO"
                    },
                    {
                        "name": "Montana",
                        "abbreviation": "MT"
                    },
                    {
                        "name": "Nebraska",
                        "abbreviation": "NE"
                    },
                    {
                        "name": "Nevada",
                        "abbreviation": "NV"
                    },
                    {
                        "name": "New Hampshire",
                        "abbreviation": "NH"
                    },
                    {
                        "name": "New Jersey",
                        "abbreviation": "NJ"
                    },
                    {
                        "name": "New Mexico",
                        "abbreviation": "NM"
                    },
                    {
                        "name": "New York",
                        "abbreviation": "NY"
                    },
                    {
                        "name": "North Carolina",
                        "abbreviation": "NC"
                    },
                    {
                        "name": "North Dakota",
                        "abbreviation": "ND"
                    },
                    {
                        "name": "Northern Mariana Islands",
                        "abbreviation": "MP"
                    },
                    {
                        "name": "Ohio",
                        "abbreviation": "OH"
                    },
                    {
                        "name": "Oklahoma",
                        "abbreviation": "OK"
                    },
                    {
                        "name": "Oregon",
                        "abbreviation": "OR"
                    },
                    {
                        "name": "Palau",
                        "abbreviation": "PW"
                    },
                    {
                        "name": "Pennsylvania",
                        "abbreviation": "PA"
                    },
                    {
                        "name": "Puerto Rico",
                        "abbreviation": "PR"
                    },
                    {
                        "name": "Rhode Island",
                        "abbreviation": "RI"
                    },
                    {
                        "name": "South Carolina",
                        "abbreviation": "SC"
                    },
                    {
                        "name": "South Dakota",
                        "abbreviation": "SD"
                    },
                    {
                        "name": "Tennessee",
                        "abbreviation": "TN"
                    },
                    {
                        "name": "Texas",
                        "abbreviation": "TX"
                    },
                    {
                        "name": "Utah",
                        "abbreviation": "UT"
                    },
                    {
                        "name": "Vermont",
                        "abbreviation": "VT"
                    },
                    {
                        "name": "Virgin Islands",
                        "abbreviation": "VI"
                    },
                    {
                        "name": "Virginia",
                        "abbreviation": "VA"
                    },
                    {
                        "name": "Washington",
                        "abbreviation": "WA"
                    },
                    {
                        "name": "West Virginia",
                        "abbreviation": "WV"
                    },
                    {
                        "name": "Wisconsin",
                        "abbreviation": "WI"
                    },
                    {
                        "name": "Wyoming",
                        "abbreviation": "WY"
                    }
                ],
            myStates: {},
            statesAndCity: {},
            seen: true,
            activeSmsinput: false,
            selectStateTrue: false,
            selectCityTrue: false,
            selectZipTrue: false,
            moverCityNameItter: {},
            moverCityName:{},
            year:{}

           /*  moverCityDefautSelected:{} */


            //isChecked: false,

        },
        computed : {
        years() {
        const year = new Date().getFullYear();
        return Array.from({length: year - 1900}, (value, index) => 1901 + index)
        },
    /*      created: function () {
    this.fetchData();
    }, */
    watch: {        
        alreadyDisallowLeadStates: function (val) {
        // this.$data.alreadyDisallowLeadStates = val;
        // console.log(val);
        // Object.keys(yourObject).length === 0

        if (val == 0) {
            // alert('hi');
            this.$data.alreadyDisallowLeadStates = val;
            console.log('hi');
        } else {
            console.log(val);
            //console.log('hi');
        }
        }
    },
    methods: {
    click: function () {
    this.show = !this.show;
    // this.activeSmsinput = !this.activeSmsinput

    },
    preStateTrue: function () {
    // console.log(this.tasks.description);
    this.preClickstate = true;
    //this.seen = false;
    },
    disallowMovingStatesWindowTrue: function () {
    this.disallowMovingStatesWindowSwitcher = true;
    },
    activeSmsinputMthd: function () {
    this.activeSmsinput = true;
    },
    fetchData: function () {
    var self = this;

            $.get(apiURL, function (data) {

                //self.myStates = data ;
                // console.log(data);
                //var arr = Object.values(data);
                /* let arr = Object.values(data);
                //console.log(arr);

                //var arr = Object.values(statesAndCity);
                //console.log(arr);
                var a = {};

                for (var k in arr) {
                    a[k] = arr[k];
                } 
                var b = JSON.stringify(a);
                self.myStates = JSON.parse(b);*/

                 self.myStates = data;

             /*    var g = self.moverCityName[self.moverStateName];
                console.log(g); */
                
               // console.log(self.moverCityName);

                // console.log(self.statesAndCity);
            });

    },
    onStateChange: function () {
       
        var a =this.myStates;
        var b = this.moverStateName;
        var c = a[b];
        var d = Object.keys(a[b].cities);
        this.moverCityNameItter = d;
            //console.log(a[b]);

        //var e = c;    

        this.moverCityName = c.cities; 
      
        //console.log(this.moverCityName); 
    // Call Json Data throug URL
       // this.fetchData();
        },      
        

    },  
   
    mounted() {   
    // Call Json Data throug URL
    this.fetchData();


    if ("@yield('disallowLeadStates')") {
    var arr = "@yield('disallowLeadStates')";
    //console.log(arr);
    //console.log(JSON.parse(arr.replace(/&quot;/g,'"')));

    var selectedStates = JSON.parse(arr.replace(/&quot;/g, '"'));
    //console.log(selectedStates);
    this.$data.alreadyDisallowLeadStates = selectedStates;

    }
    if ("@yield('disallowMovingStates')") {
    var arr = "@yield('disallowMovingStates')";
    // console.log(arr);
    //console.log(JSON.parse(arr.replace(/&quot;/g,'"')));

    var selectedStates = JSON.parse(arr.replace(/&quot;/g, '"'));
    // console.log(selectedStates);
    this.$data.alreadyDisallowMovingStates = selectedStates;

    }

    if ("@yield('moverState')") {
    var arr = "@yield('moverState')";    
    this.moverStateName = arr;
    //console.log(arr);
    }

    if ("@yield('moverCity')") {
    console.log('you are in edit');
    //this.moverCityNameItter  = Object.keys(this.moverCityName[this.moverStateName].cities);
    
    this.selectStateTrue = true;
    this.selectZipTrue = true; 

    this.moverCityDefautSelected = "@yield('moverCity')";     
    //this.moverCityDefautSelected = slCity;
    console.log(this.moverCityDefautSelected);
    }else{       
        console.log('you are in create');
    }
    if ("@yield('zipCode')") {
    var arr = "@yield('zipCode')";    
    this.moverZipcode = arr;
    console.log(arr);
    }

    if ("@yield('show')" == 'true') {
    // alert('hi');
    this.show = true,
        this.enable = true
    }
    if ("@yield('checkSmsPhn')" == 'checked') {
    //alert('hi')
    this.activeSmsinput = true

    }

    },
    
  }


    });
    // vm.fetchData();

    </script>




    @endsection
