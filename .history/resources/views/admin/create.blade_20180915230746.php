@extends('admin.layouts.app')
@section('title', 'create Mover')

@section('goBody')

 <div id="app">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand" href="#">Navbar</a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item active">
                                            <router-link to="/admin/mover/create" class="nav-link active">Mover</router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/admin/mover/example" class="nav-link">Example</router-link>
                                        </li>
                                        <li class="nav-item">
                                                <router-link to="/admin/mover/rahat" class="nav-link">rahat</router-link>
                                            </li>

                                        <li class="nav-item">
                                            <a class="nav-link disabled" href="#">Disabled</a>
                                        </li>
                                    </ul>

                                </div>
                            </nav>


                            <router-view></router-view>

                        </div>



<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="{{asset('js/app.js')}}"></script>
<script>
    //import rahat from '../../assets/js/components/rahat.vue'
    //new Vue({ el: '#components-demo' })
   
        Vue.component('rahat', {   
        template:`
        <div class='col-lg-4'>
        <ul>
            <li v-for="(item, key, index) in month"> @{{key}} -- @{{item}}</li>
        </ul>
        </div>
        `,
        props: [
        'month'
        ]
        });





    // window.Vue = require('vue')
    //var apiURL = 'https://raw.githubusercontent.com/Fyresite/US-City-State-Zip-Master-JSON/master/states.min.json';
    var apiURL = 'http://localhost:8000/admin/html/test/test.json';

    //http://localhost/admin/html/test/test.json
    //var apiURL = '../assets/data.json';
    //import json from './Views/jsonData/StateCityZip.json'
    var vm = new Vue({
        el: '#app',
        data: {
            message: 'Hello Sir!',
            userData: [],
            enable: false,
            show: false,
            preClickstate: false,
            disallowMovingStatesWindowSwitcher: false,
            state: true,
            selected: [], // Must be an array reference!
            myString: [],
            alreadyDisallowLeadStates: [],
            alreadyDisallowMovingStates: [],
            watchingSomething: [],
            statesName: [],
            moverStateName: 'selectState',
            moverCityDefautSelected: 'SelectCity',
            moverZipcode: 'selectZipCode',
            months: ["January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December"]
            ,
            states: [{
                    "name": "Alabama",
                    "abbreviation": "AL"
                },
                {
                    "name": "Alaska",
                    "abbreviation": "AK"
                },
                {
                    "name": "American Samoa",
                    "abbreviation": "AS"
                },
                {
                    "name": "Arizona",
                    "abbreviation": "AZ"
                },
                {
                    "name": "Arkansas",
                    "abbreviation": "AR"
                },
                {
                    "name": "California",
                    "abbreviation": "CA"
                },
                {
                    "name": "Colorado",
                    "abbreviation": "CO"
                },
                {
                    "name": "Connecticut",
                    "abbreviation": "CT"
                },
                {
                    "name": "Delaware",
                    "abbreviation": "DE"
                },
                {
                    "name": "District Of Columbia",
                    "abbreviation": "DC"
                },
                {
                    "name": "Federated States Of Micronesia",
                    "abbreviation": "FM"
                },
                {
                    "name": "Florida",
                    "abbreviation": "FL"
                },
                {
                    "name": "Georgia",
                    "abbreviation": "GA"
                },
                {
                    "name": "Guam",
                    "abbreviation": "GU"
                },
                {
                    "name": "Hawaii",
                    "abbreviation": "HI"
                },
                {
                    "name": "Idaho",
                    "abbreviation": "ID"
                },
                {
                    "name": "Illinois",
                    "abbreviation": "IL"
                },
                {
                    "name": "Indiana",
                    "abbreviation": "IN"
                },
                {
                    "name": "Iowa",
                    "abbreviation": "IA"
                },
                {
                    "name": "Kansas",
                    "abbreviation": "KS"
                },
                {
                    "name": "Kentucky",
                    "abbreviation": "KY"
                },
                {
                    "name": "Louisiana",
                    "abbreviation": "LA"
                },
                {
                    "name": "Maine",
                    "abbreviation": "ME"
                },
                {
                    "name": "Marshall Islands",
                    "abbreviation": "MH"
                },
                {
                    "name": "Maryland",
                    "abbreviation": "MD"
                },
                {
                    "name": "Massachusetts",
                    "abbreviation": "MA"
                },
                {
                    "name": "Michigan",
                    "abbreviation": "MI"
                },
                {
                    "name": "Minnesota",
                    "abbreviation": "MN"
                },
                {
                    "name": "Mississippi",
                    "abbreviation": "MS"
                },
                {
                    "name": "Missouri",
                    "abbreviation": "MO"
                },
                {
                    "name": "Montana",
                    "abbreviation": "MT"
                },
                {
                    "name": "Nebraska",
                    "abbreviation": "NE"
                },
                {
                    "name": "Nevada",
                    "abbreviation": "NV"
                },
                {
                    "name": "New Hampshire",
                    "abbreviation": "NH"
                },
                {
                    "name": "New Jersey",
                    "abbreviation": "NJ"
                },
                {
                    "name": "New Mexico",
                    "abbreviation": "NM"
                },
                {
                    "name": "New York",
                    "abbreviation": "NY"
                },
                {
                    "name": "North Carolina",
                    "abbreviation": "NC"
                },
                {
                    "name": "North Dakota",
                    "abbreviation": "ND"
                },
                {
                    "name": "Northern Mariana Islands",
                    "abbreviation": "MP"
                },
                {
                    "name": "Ohio",
                    "abbreviation": "OH"
                },
                {
                    "name": "Oklahoma",
                    "abbreviation": "OK"
                },
                {
                    "name": "Oregon",
                    "abbreviation": "OR"
                },
                {
                    "name": "Palau",
                    "abbreviation": "PW"
                },
                {
                    "name": "Pennsylvania",
                    "abbreviation": "PA"
                },
                {
                    "name": "Puerto Rico",
                    "abbreviation": "PR"
                },
                {
                    "name": "Rhode Island",
                    "abbreviation": "RI"
                },
                {
                    "name": "South Carolina",
                    "abbreviation": "SC"
                },
                {
                    "name": "South Dakota",
                    "abbreviation": "SD"
                },
                {
                    "name": "Tennessee",
                    "abbreviation": "TN"
                },
                {
                    "name": "Texas",
                    "abbreviation": "TX"
                },
                {
                    "name": "Utah",
                    "abbreviation": "UT"
                },
                {
                    "name": "Vermont",
                    "abbreviation": "VT"
                },
                {
                    "name": "Virgin Islands",
                    "abbreviation": "VI"
                },
                {
                    "name": "Virginia",
                    "abbreviation": "VA"
                },
                {
                    "name": "Washington",
                    "abbreviation": "WA"
                },
                {
                    "name": "West Virginia",
                    "abbreviation": "WV"
                },
                {
                    "name": "Wisconsin",
                    "abbreviation": "WI"
                },
                {
                    "name": "Wyoming",
                    "abbreviation": "WY"
                }
            ],
            myStates: {},
            statesAndCity: {},
            seen: true,
            activeSmsinput: false,
            selectStateTrue: false,
            selectCityTrue: false,
            selectZipTrue: false,
            moverCityNameItter: {},
            moverCityName: {},
            creatEdit: false
            /*  moverCityDefautSelected:{} */


            //isChecked: false,

        },
        computed: {
            years() {
                const year = new Date().getFullYear()
                return Array.from({
                    length: year - 1900
                }, (value, index) => 1901 + index)
            }
        },
        /*      created: function () {
        this.fetchData();
        }, */
        watch: {
            alreadyDisallowLeadStates: function (val) {
                // this.$data.alreadyDisallowLeadStates = val;
                // console.log(val);
                // Object.keys(yourObject).length === 0

                if (val == 0) {
                    // alert('hi');
                    this.$data.alreadyDisallowLeadStates = val;
                    console.log('hi');
                } else {
                    console.log(val);
                    //console.log('hi');
                }
            }
        },
        methods: {
            click: function () {
                this.show = !this.show;
                // this.activeSmsinput = !this.activeSmsinput

            },
            preStateTrue: function () {
                // console.log(this.tasks.description);
                this.preClickstate = true;
                //this.seen = false;
            },
            disallowMovingStatesWindowTrue: function () {
                this.disallowMovingStatesWindowSwitcher = true;
            },
            activeSmsinputMthd: function () {
                this.activeSmsinput = true;
            },
            fetchData: function () {
                var self = this;

                $.get(apiURL, function (data) {

                    //self.myStates = data ;
                    // console.log(data);
                    //var arr = Object.values(data);
                    /* let arr = Object.values(data);
                    //console.log(arr);

                    //var arr = Object.values(statesAndCity);
                    //console.log(arr);
                    var a = {};

                    for (var k in arr) {
                        a[k] = arr[k];
                    } 
                    var b = JSON.stringify(a);
                    self.myStates = JSON.parse(b);*/

                    self.myStates = data;

                    /*    var g = self.moverCityName[self.moverStateName];
                       console.log(g); */

                    // console.log(self.moverCityName);

                    // console.log(self.statesAndCity);
                });

            },
            onStateChange: function () {

                var a = this.myStates;
                var b = this.moverStateName;
                var c = a[b];
                var d = Object.keys(a[b].cities);
                this.moverCityNameItter = d;
                //console.log(a[b]);

                //var e = c;    

                this.moverCityName = c.cities;

                //console.log(this.moverCityName); 
                // Call Json Data throug URL
                // this.fetchData();
            }

        },
        mounted() {
            // Call Json Data throug URL
            this.fetchData();


            if ("@yield('disallowLeadStates')") {
                var arr = "@yield('disallowLeadStates')";
                //console.log(arr);
                //console.log(JSON.parse(arr.replace(/&quot;/g,'"')));

                var selectedStates = JSON.parse(arr.replace(/&quot;/g, '"'));
                //console.log(selectedStates);
                this.$data.alreadyDisallowLeadStates = selectedStates;

            }
            if ("@yield('disallowMovingStates')") {
                var arr = "@yield('disallowMovingStates')";
                // console.log(arr);
                //console.log(JSON.parse(arr.replace(/&quot;/g,'"')));

                var selectedStates = JSON.parse(arr.replace(/&quot;/g, '"'));
                // console.log(selectedStates);
                this.$data.alreadyDisallowMovingStates = selectedStates;

            }

            if ("@yield('moverState')") {
                var arr = "@yield('moverState')";
                this.moverStateName = arr;
                //console.log(arr);
            }

            if ("@yield('moverCity')") {
                console.log('you are in edit');
                //this.moverCityNameItter  = Object.keys(this.moverCityName[this.moverStateName].cities);
                this.creatEdit = true;
                this.selectStateTrue = true;
                this.selectZipTrue = true;

                this.moverCityDefautSelected = "@yield('moverCity')";
                //this.moverCityDefautSelected = slCity;
                console.log(this.moverCityDefautSelected);
            } else {
                console.log('you are in create');
            }
            if ("@yield('zipCode')") {
                var arr = "@yield('zipCode')";
                this.moverZipcode = arr;
                console.log(arr);
            }

            if ("@yield('show')" == 'true') {
                // alert('hi');
                this.show = true,
                    this.enable = true
            }
            if ("@yield('checkSmsPhn')" == 'checked') {
                //alert('hi')
                this.activeSmsinput = true

            }
        },


    });
    // vm.fetchData();

</script>




@endsection
