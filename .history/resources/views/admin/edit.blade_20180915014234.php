@extends('admin.create')
@section('editId', $item->id)

@section('userName', $item->userName)
@section('userPassword', $item->password)
@section('moverName', $item->moverName)
@section('usDotNumber', $item->usDotNumber)
@section('ownerName', $item->ownerName)

@section('ownerMccNum', $item->ownerMccNum)
@section('contactName', $item->contactName)
@section('otherLcnInfo', $item->otherLcnInfo)
@section('gnrlEmail', $item->gnrlEmail)
@section('leadCollectEmail', $item->leadCollectEmail)


@if($item->chkFrmtHtml == 1) ? @section('chkFrmtHtml', $item->chkFrmtHtml='checked') : @section('plain', $item->chkFrmtHtml='unchecked') @endif
@if($item->chkFrmtHtml == 0) ? @section('plain', $item->chkFrmtHtml='checked') : @section('chkFrmtHtml', $item->chkFrmtHtml='unchecked') @endif


{{--@if($item->chkSendList == NULL)

    @section('chkFrmtHtml', $item->chkFrmtHtml='uncheck')
    @section('plain', $item->chkFrmtHtml='uncheck')


@elseif($item->chkSendList == 'checked')
    @section('chkSendList', $item->chkSendList)
@if($item->chkFrmtHtml == 1) ? @section('chkFrmtHtml', $item->chkFrmtHtml='checked') : @section('plain', $item->chkFrmtHtml='unchecked') @endif
@if($item->chkFrmtHtml == 0) ? @section('plain', $item->chkFrmtHtml='checked') : @section('chkFrmtHtml', $item->chkFrmtHtml='unchecked') @endif
@endif--}}
{{--if($item->chkFrmtHtml == 1){
@section('chkFrmtHtml', $item->chkFrmtHtml='checked')

}
else{
@section('plain', $item->chkFrmtHtml='checked')

}--}}
{{--@if($item->chkFrmtHtml == 1) ? checked : no @endif
@if($item->chkFrmtHtml == 0) ? no : checked @endif

@section('chkFrmtHtml', $item->chkFrmtHtml)--}}
{{--@section('chkFrmtPlain', $item->chkFrmtPlain)--}}


@section('checkSmsPhn', $item->checkSmsPhn)
@section('smsNumber', $item->smsNumber)

@section('chkGranot', $item->chkGranot)
@section('longLeadPrice', $item->longLeadPrice)
@section('localLeadPrice', $item->localLeadPrice)
@section('leadPerHour', $item->leadPerHour)

@section('chkcarleadsEmail', $item->chkcarleadsEmail)
@section('chkcarleadsGranot', $item->chkcarleadsGranot)
@section('longCarLeadPrice', $item->longCarLeadPrice)
@section('localCarLeadPrice', $item->localCarLeadPrice)

@php
    $stateSelected = json_decode($item->disallowLeadStates);
    //$stateSelected = $item->disallowLeadStates;
    //echo $stateSelected;
@endphp
{{--@foreach($stateSelected as $value){{$value}}, @endforeach--}}

{{--@if(in_array('Delaware', $stateSelected))
    Output String
@endif--}}


@section('disallowLeadStates', $item->disallowLeadStates)
@section('disallowMovingStates', $item->disallowMovingStates)

@section('moverAdd', $item->moverAdd)

@section('moverState', $item->moverState)
@section('moverCity', $item->moverCity)
@section('moverPhoneNumber', $item->moverPhoneNumber)

@section('moverFaxNumber', $item->moverFaxNumber)

@section('chkPaypal', $item->chkPaypal)
@section('chkCc', $item->chkCc)

@section('cardHolderName', $item->cardHolderName)

@section('cardExpMonth', $item->cardExpMonth)
@section('ccNumber', $item->ccNumber)
@section('cardExpYear', $item->cardExpYear)
@section('cwNumber', $item->cwNumber)


@if($item->chkSendList == 'checked')
    @section('show', 'true')

@else
    @section('show', 'false')
{{--@section('show', false)--}}
@endif

{{--@if($item->checkSmsPhn == 'checked')
    @section('show', 'true')

@else
    @section('show', 'false')
--}}{{--@section('show', false)--}}{{--
@endif--}}


@section('detailsCustomer', $item->detailsCustomer)
@section('editMethod')
    {{method_field('PUT')}}
@endsection

