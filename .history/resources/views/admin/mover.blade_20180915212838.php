@extends('admin.layouts.app')

@section('goBody')
<br>
@if(session()->has('messege'))
<div class="alert alert-success" role="alert">
    <strong>{{session()->get('messege')}}</strong>
</div>
@endif


<br>
<h1 class="text-center bg-secondary text-white">Home Mover List</h1>
 <div id="app">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <router-link to="/mover" class="nav-link active">Mover</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link to="/example" class="nav-link">Example</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link to="/rahat" class="nav-link">rahat</router-link>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>
                    </ul>

                </div>
            </nav>


            <router-view></router-view>

        </div>

<div class="container">


    <div class="col-md-6 col-md-offset-3">
        <div class="row">
            <ul class="list-group col-md-8">

                {{--<div id="app">

                    --}}{{--@foreach($movers as $mover)

                    <li class="list-group-item">
                        {{$mover->moverName}}
                    </li>

                    @endforeach--}}{{--
                    <mover userNmae='userName'></mover>
                </div>--}}

                {{--<li class="bg-{{$bgcol}}">hi</li>--}}
                @foreach($movers as $i=>$mover)
                @php
                $colors = ['danger','warning','success','info','secondary','dark'];
                //$randomize = $colors[mt_rand(0, count($colors)-1)];
                $index = array_rand($colors, 1);
                $selected = $colors[$index];
                unset($colors[$index]);
                @endphp
                <li class="list-group-item bg-{{$selected}}">
                    <a class="text-white" href="{{'/admin/mover/'.$mover->id}}">{{$mover->moverName}}</a>
                    <span class="pull-right">{{$mover->created_at->diffforHumans()}}</span>

                </li>

                @endforeach
            </ul>
            <ul class="list-group col-md-4">
                @foreach($movers as $mover)

                <li class="list-group-item">
                    <a href="{{'/admin/mover/'.$mover->id.'/edit'}}"><i class="far fa fa-edit"></i></a>
                    <form class="pull-right" action="{{'/admin/mover/'.$mover->id}}" method="post">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button type="submit" style="border: none;padding: 0;background-color: snow;"><i class="fas fa fa-trash text-success"></i></button>
                    </form>
                </li>

                @endforeach
            </ul>
        </div>
      {{--   <div id="app">
            <router-link to="/admin/mover" class="nav-link">rahat</router-link>
            <rahat :month="months"></rahat>
        </div> --}}
       
    </div>


</div>

@endsection
@section('footer')
<script src="{{asset('js/app.js')}}"></script>
@endsection
