@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        {{-- @if (session('status'))
                             <div class="alert alert-success" role="alert">
                                 {{ session('status') }}

                             </div>
                         @endif--}}

                        <div id="app">
                           {{--  <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand" href="#">Navbar</a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item active">
                                            <router-link to="/mover" class="nav-link active">Mover</router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/example" class="nav-link">Example</router-link>
                                        </li>
                                        <li class="nav-item">
                                                <router-link to="/rahat" class="nav-link">rahat</router-link>
                                            </li>

                                        <li class="nav-item">
                                            <a class="nav-link disabled" href="#">Disabled</a>
                                        </li>
                                    </ul>

                                </div>
                            </nav> --}}


                            <router-view></router-view>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
