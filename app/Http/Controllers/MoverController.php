<?php

namespace App\Http\Controllers;
use App\Model\admin\mover;

use Illuminate\Http\Request;


class MoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movers = mover::all();
        return view('admin.mover', compact('movers'));
        // return mover::pluck('id');

    }

    /* public function getallMovers()
     {
         $movers = mover::all();
         return view('admin.mover', compact('movers'));
         // return mover::pluck('id');

     }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $movers = mover::all();
        return view('admin.create', compact('movers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return 'hi';
        $mover = new mover;

        $mover->userName = $request->userName;
        $mover->password = bcrypt('$request->password');
        $mover->moverName = $request->moverName;
        $mover->usDotNumber = $request->usDotNumber;
        $mover->ownerName = $request->ownerName;

        $mover->ownerMccNum = $request->ownerMccNum;
        $mover->contactName = $request->contactName;
        $mover->otherLcnInfo = $request->otherLcnInfo;
        $mover->gnrlEmail = $request->gnrlEmail;
        $mover->leadCollectEmail = $request->leadCollectEmail;


        $mover->chkSendList = $request->chkSendList;
        $mover->chkFrmtHtml = $request->chkFrmtHtml;
        /* $mover->chkFrmtPlain = $request->chkFrmtPlain;*/
        $mover->checkSmsPhn = $request->checkSmsPhn;
        $mover->smsNumber = $request->smsNumber;

        $mover->chkGranot = $request->chkGranot;
        $mover->longLeadPrice = $request->longLeadPrice;
        $mover->localLeadPrice = $request->localLeadPrice;
        $mover->leadPerHour = $request->leadPerHour;

        $mover->chkcarleadsEmail = $request->chkcarleadsEmail;
        $mover->chkcarleadsGranot = $request->chkcarleadsGranot;
        $mover->longCarLeadPrice = $request->longCarLeadPrice;
        $mover->localCarLeadPrice = $request->localCarLeadPrice;

        $mover->disallowLeadStates = $request->disallowLeadStates;
        $mover->disallowMovingStates = $request->disallowMovingStates;

        $mover->moverAdd = $request->moverAdd;
        $mover->zipCode = $request->zipCode;

        $mover->moverCity = $request->moverCity;
        $mover->moverPhoneNumber = $request->moverPhoneNumber;
        $mover->moverState = $request->moverState;
        $mover->moverFaxNumber = $request->moverFaxNumber;

        $mover->chkPaypal = $request->chkPaypal;
        $mover->chkCc = $request->chkCc;

        $mover->cardHolderName = $request->cardHolderName;

        $mover->cardExpMonth = $request->cardExpMonth;
        $mover->ccNumber = $request->ccNumber;
        $mover->cardExpYear = $request->cardExpYear;
        $mover->cwNumber = $request->cwNumber;
        $mover->detailsCustomer = $request->detailsCustomer;

        /* $mover->title = $request->title;
        $mover->body = $request->body;*/

//return $request->all();
        $mover->save();
        return redirect('admin/mover');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = mover::find($id);
        return view('admin.show', compact('item'));
        //return $item->disallowLeadStates;


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = mover::find($id);


       /*  $finalArray = array();        
        $arr = array(
       
           "CA" => array(
               "FullName" => "California",
               "CityList" => array(
                   "Los Angelse" => array(
                       "CityName" => "Los Angeles",
                       "Zips" => array(
                           '123','453244','234234'
                       )
                       ),
                   "Los Angelse 2" => array(
                       "CityName" => "Los Angeles 2",
                       "Zips" => array(
                           '92821','92821','92821'
                       )
                       ),
       
                       ) 
                   ),
       
           "Ny" => array(
               "FullName" => "New York",
               "CityList" => array(
                   "Brooklyen" => array(
                       "CityName" => "Brooklyne",
                       "Zips" => array(
                           '234234','456456','567567'
                       )
                       ),
                   "NewYork city" => array(
                       "CityName" => "NewYork city",
                       "Zips" => array(
                           '11220','11221','11223'
                       )
                       ),
       
                       ) 
                   ),
                   
       
               );
       
        
       $j = json_encode($arr);
       echo $j;
       exit; */




        return view('admin.edit', compact('item'));


        //  return 'hi';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mover = mover::find($id);

       
        
        
        /*  $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
        ]); 






      $mover->title = $requ2->title;

       echo "<pre>"; print_r($mover); echo "</pre>";
      
      $mover->body 3re

      echo "<pre>"; print_r($mover); echo "</pre>";
        
        exit;quest->body;*/

        $mover->userName = $request->userName;
        $mover->password = $request->password;
        $mover->moverName = $request->moverName;
        $mover->usDotNumber = $request->usDotNumber;
        $mover->ownerName = $request->ownerName;

        $mover->ownerMccNum = $request->ownerMccNum;
        $mover->contactName = $request->contactName;
        $mover->otherLcnInfo = $request->otherLcnInfo;
        $mover->gnrlEmail = $request->gnrlEmail;
        $mover->leadCollectEmail = $request->leadCollectEmail;

        $mover->chkSendList = $request->chkSendList;
        $mover->chkFrmtHtml = $request->chkFrmtHtml;
        /*$mover->chkFrmtPlain = $request->chkFrmtPlain;*/
        $mover->checkSmsPhn = $request->checkSmsPhn;
        $mover->smsNumber = $request->smsNumber;

        $mover->chkGranot = $request->chkGranot;
        $mover->longLeadPrice = $request->longLeadPrice;
        $mover->leadPerHour = $request->leadPerHour;

        $mover->chkcarleadsEmail = $request->chkcarleadsEmail;
        $mover->chkcarleadsGranot = $request->chkcarleadsGranot;
        $mover->longCarLeadPrice = $request->longCarLeadPrice;
        $mover->localCarLeadPrice = $request->localCarLeadPrice;


        $mover->disallowLeadStates = $request->disallowLeadStates;
        $mover->disallowMovingStates = $request->disallowMovingStates;

        $mover->moverAdd = $request->moverAdd;
        $mover->zipCode = $request->zipCode;

        $mover->moverCity = $request->moverCity;
        $mover->moverPhoneNumber = $request->moverPhoneNumber;
        $mover->moverState = $request->moverState;
        $mover->moverFaxNumber = $request->moverFaxNumber;

        $mover->chkPaypal = $request->chkPaypal;
        $mover->chkCc = $request->chkCc;

        $mover->cardExpMonth = $request->cardExpMonth;
        $mover->ccNumber = $request->ccNumber;
        $mover->cardExpYear = $request->cardExpYear;
        $mover->cwNumber = $request->cwNumber;
        $mover->detailsCustomer = $request->detailsCustomer;


        //return $request->all();

        $mover->save();
        session()->flash('messege', 'Updated Successfully');
       return redirect('admin/mover');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mover = mover::find($id);
        $mover->delete();
        session()->flash('messege', 'Deleted Successfully');
        return redirect('admin/mover');
    }
}
