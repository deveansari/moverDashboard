<?php

namespace App\Model\admin;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Crypt;

use Illuminate\Database\Eloquent\Model;

class mover extends Model
{


    public function setdisallowLeadStatesAttribute($value)
    {
        if ($value) {
            $this->attributes['disallowLeadStates'] = json_encode($value);
        } else {
            $this->attributes['disallowLeadStates'] = $value;
        }

    }
    /*   public function getdisallowLeadStatesAttribute($value)
    {
        //pr($value);
        //pr(serialize($value));
        return serialize($value);

    } */
    public function setdisallowMovingStatesAttribute($value)
    {
        if ($value) {
            $this->attributes['disallowMovingStates'] = json_encode($value);
        } else {
            $this->attributes['disallowMovingStates'] = $value;
        }

    }

    public function setcwNumberAttribute($value)

    {

        if ($value) {
            $firstTelvDigitHolder = substr($value, 0, 12);
            //$firstTelvDigit = ($firstTelvDigitHolder / 2);
            // $firstTelvDigit = Crypt::encryptString('$firstTelvDigitHolder');
            $lastFourDigit = substr($value, -4);
            $encoDed = base64_encode($firstTelvDigitHolder);
            // return $firstTelvDigit;
            //$bothPartHolder = $firstTelvDigit.$lastFourDigit;


            $this->attributes['cwNumber'] = $encoDed . "-" . $lastFourDigit;
            // $this->attributes['cwNumber'] = $encoDed;

        } else {
            $this->attributes['cwNumber'] = $value;
        }

    }

    public function getcwNumberAttribute($value)
    {
        
        $firstTelvDigitHolder = substr($value, 0, 12);
        $lastFourDigit = substr($value, -4);

        $firstTelvDigit = substr_replace($firstTelvDigitHolder, "************", 0);;
        //$firstTelvDigit = base64_decode($firstTelvDigitHolder);

        return $this->attributes['cwNumber'] = $firstTelvDigit . " - " . $lastFourDigit;
        //return $this->attributes['cwNumber'] = $value;


    }

}
