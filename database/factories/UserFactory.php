<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Model\admin\mover::class, function (Faker $faker) {
    return [

        'userName' => $faker->name,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
        'moverName' => $faker->company,
        'usDotNumber' => $faker->randomDigit,
        'ownerName' => $faker->name,

        'ownerMccNum' => $faker->randomDigit,
        'contactName' => $faker->name,
        'otherLcnInfo' => $faker->ssn,
        'gnrlEmail' => $faker->email,
        'leadCollectEmail' => $faker->email,

        'chkSendList' => $faker->boolean,
        'chkFrmtHtml' => $faker->boolean,
        'checkSmsPhn' => $faker->boolean,
        'smsNumber' => $faker->randomNumber(7),
        'chkGranot' => $faker->boolean,

        'longLeadPrice' => $faker->randomNumber(3),
        'leadPerHour' => $faker->randomNumber(2),

        'chkcarleadsEmail' => $faker->boolean,
        'chkcarleadsGranot' => $faker->boolean,

        'longCarLeadPrice' => $faker->randomNumber(3),
        'localCarLeadPrice' => $faker->randomNumber(3),
        'disallowLeadStates' => $faker->randomElement($array = array (["AL","LA"])),
        'disallowMovingStates' => $faker->randomElement($array = array ('a','b','c')),

        'moverAdd' => $faker->address,
        'zipCode' => $faker->postcode,

        'moverCity' => $faker->city,
        'moverPhoneNumber' => $faker->phoneNumber,
        'moverState' => $faker->state,
        'moverFaxNumber' => $faker->phoneNumber,

        'chkPaypal' => $faker->boolean,
        'chkCc' => $faker->boolean,

        'cardExpMonth' => $faker->monthName($max = 'now'),
        'ccNumber' => $faker->creditCardNumber,
        'cardExpYear' => $faker->year($max = 'now'),
        'cwNumber' => $faker->creditCardNumber,
        'detailsCustomer' => $faker->sentence($nbWords = 6, $variableNbWords = true),

    ];
});
