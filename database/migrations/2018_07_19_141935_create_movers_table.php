<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userName')->nullable();
            $table->string('password')->nullable();
            $table->string('moverName')->nullable();
            $table->integer('usDotNumber')->nullable();
            $table->string('ownerName')->nullable();

            $table->integer('ownerMccNum')->nullable();
            $table->string('contactName')->nullable();
            $table->string('otherLcnInfo')->nullable();
            $table->string('gnrlEmail')->nullable();
            $table->string('leadCollectEmail')->nullable();

            $table->string('chkSendList')->nullable();
            $table->boolean('chkFrmtHtml')->nullable();
            /*$table->boolean('chkFrmtPlain')->nullable();*/
            $table->string('checkSmsPhn')->nullable();
            $table->bigInteger('smsNumber')->nullable();

            $table->string('chkGranot')->nullable();
            $table->string('longLeadPrice')->nullable();
            $table->string('localLeadPrice')->nullable();
            $table->string('leadPerHour')->nullable();

            $table->string('chkcarleadsEmail')->nullable();
            $table->string('chkcarleadsGranot')->nullable();
            $table->string('longCarLeadPrice')->nullable();
            $table->string('localCarLeadPrice')->nullable();
            $table->string('disallowLeadStates')->nullable();
            $table->string('disallowMovingStates')->nullable();

            $table->string('moverAdd')->nullable();
            $table->string('zipCode')->nullable();

            $table->string('moverCity')->nullable();
            $table->string('moverPhoneNumber')->nullable();
            $table->string('moverState')->nullable();
            $table->string('moverFaxNumber')->nullable();

            $table->string('chkPaypal')->nullable();
            $table->string('chkCc')->nullable();

            $table->string('cardHolderName')->nullable();

            $table->string('cardExpMonth')->nullable();
            $table->bigInteger('ccNumber')->nullable();
            $table->string('cardExpYear')->nullable();
            $table->string('cwNumber')->nullable();
            $table->string('detailsCustomer')->nullable();



          /*  $table->string('title');
            $table->string('body');*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movers');
    }
}
