<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(App\Model\admin\CreateMoversTable::class);

        //factory(app\Model\admin\mover::class, 50)->create();
        /* DB::table('users')->insert([
             'name' => str_random(10),
             'email' => str_random(10).'@gmail.com',
             'password' => bcrypt('secret'),
         ]);

         });*/
        $this->call(MoverSeeder::class);


    }
}
