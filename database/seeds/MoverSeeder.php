<?php

use Illuminate\Database\Seeder;

class MoverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Model\admin\mover::class, 2)->create();
    }
}
