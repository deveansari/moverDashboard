<!doctype html>
<html lang="en">
<head>
    @include('admin.layouts.head')
</head>
<body>
@include('admin.layouts.header')
<div id="wrapper">

@include('admin.layouts.sidebar')

<!-- End #right-sidebar -->

    <!--Body content-->

    <div id="content" class="page-content clearfix">
        <div class="contentwrapper">


            @section('goBody')
            @show

            @include('admin.layouts.footer')
        </div>

    </div>


</div>

</body>
</html>