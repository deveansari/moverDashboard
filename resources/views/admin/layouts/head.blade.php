<meta charset="utf-8">
<title>@yield('title', 'Dashboard') | Supr Admin Template</title>
<!-- Mobile specific metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Force IE9 to render in normal mode -->
<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
<meta name="author" content="" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="application-name" content="" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<!-- Import google fonts - Heading first/ text second -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet" type="text/css">
{{--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet" type="text/css">--}}
<!-- Css files -->
<!-- Icons -->
<link href="{{asset('admin/html/css/icons.css')}}" rel="stylesheet" />
<!-- Bootstrap stylesheets (included template modifications) -->
<link href="{{asset('admin/html/css/bootstrap.css')}}" rel="stylesheet" />
<!-- Plugins stylesheets (all plugin custom css) -->
<link href="{{asset('admin/html/css/plugins.css')}}" rel="stylesheet" />
<!-- Main stylesheets (template main css file) -->
<link href="{{asset('admin/html/css/main.css')}}" rel="stylesheet" />
<!-- Custom stylesheets ( Put your own changes here ) -->

<link href="{{asset('admin/html/css/custom.css')}}" rel="stylesheet" />


{{--<link href="{{asset('../node_modules/summernote/dist/summernote-bs4.css')}}" rel="stylesheet">--}}
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('admin/html/img/ico/apple-touch-icon-144-precomposed.png')}}">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('admin/html/img/ico/apple-touch-icon-114-precomposed.png')}}">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('admin/html/img/ico/apple-touch-icon-72-precomposed.png')}}">
<link rel="apple-touch-icon-precomposed" href="{{asset('admin/html/')}}img/ico/apple-touch-icon-57-precomposed.png">
<link rel="icon" href="{{asset('admin/html/img/ico/favicon.ico')}}" type="image/png">
<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name="msapplication-TileColor" content="#3399cc" />
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--}}
{{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.2/flatly/bootstrap.min.css">--}}

{{--
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">--}}
{{--<script src="{{asset('/path/to/vue.js')}}"></script>
<script src="{{asset('/path/to/vue-router.js')}}"></script>--}}

<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>

@section('gohead')
    @show

