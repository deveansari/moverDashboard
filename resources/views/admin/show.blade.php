@extends('admin.layouts.app')
@section('title', 'Inside mover')
@section('goBody')
    <br>
    <h5 class="text-center text-info">Inside mover table through id</h5>

    <div class="addItem text-right">
        <a href="/admin/mover" class="btn btn-info"><i class="fa fa-arrow-left"></i></a>
    </div>
    <hr>
    {{--{{dd($item->toArray())}}--}}
    <div class="row">
        <div class="col-lg-12">
            <!-- col-lg-12 start here -->
            <div class="panel panel-default toggle panelMove panelClose panelRefresh">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title">Basic Data tables</h4>
                </div>
                <div class="panel-body">
                    <table id="basic-datatables" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User Name</th>
                            <th>User Name</th>
                            <th>Body</th>
                            <th>disallow state</th>
                        </tr>
                        </thead>

                        <tbody>
                        <td>{{$item->id}}</td>
                        <td>{{$item->userName}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->body}}</td>
                        <td>{{$item->disallowLeadStates}}</td>


                        </tbody>

                    </table>
                 
                    <a href="{{'/admin/mover/'.$item->id.'/edit'}}"><i class="far fa fa-edit"></i></a>
                </div>
            </div>
            <!-- End .panel -->
        </div>
        <!-- col-lg-12 end here -->


    </div>
    <div class="container">

        <div class="row">

            <div class="col-md-4 col-md-offset-4">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <div class="d-flex w-100">
                            <h5 class="mb-1">title: {{$item->moverName}}</h5>
                            {{--<small></small>--}}
                        </div>
                        <p class="mb-1">userName: {{$item->ownerName}}</p>
                        <p class="mb-1">body: {{$item->detailsCustomer}}</p>

                        <small>{{$item->created_at->diffforHumans()}}</small>
                        {{--{{dd($item)}}--}}
                        {{--{{dd($item->toArray())}}--}}
                        {{--  @php
                           $collection = collect('item');

                           $collection->transform(function ($item, $key) {
                           return $item * 2;
                           });

                           $collection->all();
                       @endphp--}}

                    </a>
                    @php

                    @endphp

                    <p>{{$item}}</p>

                </div>
            </div>

        </div>


    </div>
@endsection

